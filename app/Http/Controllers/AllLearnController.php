<?php

namespace App\Http\Controllers;

use App\Providers\AppServiceProvider;
use Illuminate\Http\Request;
use Redirect;

use App\category;
use App\language;
use App\courses;

use App\news;


class AllLearnController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // protected $test;

    public function __construct()
    {
       // $this->test = $test;

         // $this->middleware('auth');

                //$this->middleware(['auth','verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {



$lang = \Mcamara\LaravelLocalization\Facades\LaravelLocalization::getCurrentLocale();
@$get_lang = language::where('langcode','=',$lang)->get();
@$get_lang_id = $get_lang->first()->id;


@$get_category_news = category::where('lang_id','=',$get_lang_id)->get();


$learns =  courses::where('lang_id','=',$get_lang_id)
->orderBy('created_at', 'desc')
->paginate(6);

//dd($learns);

@$title = $learns->first()->title;
//dd($learns);

 //dd($get_news);


      return view('website.pages-front.AllLearn.index',compact('title','learns'));


    }
}
