<?php

namespace App\Http\Controllers;

use App\Providers\AppServiceProvider;
use Illuminate\Http\Request;
use Redirect;

use App\category;
use App\language;

use App\report;


class AllReportsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // protected $test;

    public function __construct()
    {
       // $this->test = $test;

         // $this->middleware('auth');

                //$this->middleware(['auth','verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {



$lang = \Mcamara\LaravelLocalization\Facades\LaravelLocalization::getCurrentLocale();
@$get_lang = language::where('langcode','=',$lang)->get();
@$get_lang_id = $get_lang->first()->id;


@$get_category_news = category::where('lang_id','=',$get_lang_id)->get();
// dd($get_category_news);

$title = "all  Reports";
$storys = report::where('lang_id','=',$get_lang_id)->orderBy('id', 'DESC')->get();




      return view('website.pages-front.Reports.index',compact('title','storys'));


    }



    public function Details($id)
    {
        //
        // dd('yousry');
        // return view('website.front.contact.index',compact('users'));
        $storys = report::where('id','=',$id)->get();
        // dd($storys);
       $title = $storys->first()->name;
        return view('website.pages-front.Reports.Details',compact('storys','title'));

    }
}
