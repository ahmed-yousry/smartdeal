<?php

namespace App\Http\Controllers\AdminController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\image;

use Helper;
class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
         $news = image::all();
         //dd($news);
         return view('admin.Image.index',compact('news'));
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create()
     {
         // if (Auth::guard('admin')->user()->can('admins.create')) {
         //     $roles = Role::all();
         //     return view('admin.manager.create',compact('roles'));
         // }
         // return redirect()->back();


              return view('admin.Image.create');

     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(Request $request)
     {

       // dd($request);
         // $this->validate($request,[
         //     'username' => 'required|string|max:255',
         //     'email' => 'required|string|max:255|unique:admins',
         //     'password' =>'required|string|max:10|confirmed',
         //     'role' => 'required'
         // ]);

   //         $request['password'] = bcrypt($request->password);
   //         $admin = Admin::create($request->all());
   //
   //         $admin->roles()->sync($request->role);
   // //        Admin::find($id)->roles()->sync($request->role);

        $news = new Image();

      // $width=  $news->width= $request->width;
      //   $hight= $news->hight = $request->hight;
        $news->name = Helper::uploadImageNormal($request->file('image'),'uplodes/newsphoto/');



       $news->save();
         return redirect('/admin/image')->withFlashMessage('Learn has been Stored');

     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id)
     {
         //
         // dd($id);
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id)
     {
         // if (Auth::guard('admin')->user()->can('admins.update')) {
         //     $manager = Admin::find($id);
         //     $roles = Role::all();
         //     return view('admin.manager.edit',compact('roles','manager'));
         // }
         // return redirect()->back();

         $news = Image::where('id','=',$id)->get();

         // dd($news);
         return view('admin.Image.edit',compact('news'));


     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request, $id)
     {
         // $this->validate($request,[
         //     'username' => 'required|string|max:255',
         //     'email' => 'required|string|max:255',
         // ]);
         // $manager = news::find($id)->update($request->except('_token','_method'));
         // return redirect('/admin/news')->withFlashMessage('News Edited');






         $news= Image::find($id);

        // $width=  $news->width= $request->width;
        //  $hight = $news->hight = $request->hight;
         if($file = $request->file('image')){
             @unLink(base_path().'/public/uplodes/newsphoto/'.$news->image);
             $news->name = Helper::uploadImageNormal($request->file('image'),'uplodes/newsphoto/');
         }else{
             $news->name = $news->image;
         }


         $news->save();

         return redirect('/admin/image')->withFlashMessage('Learn Edited !!');

     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id)
     {
         // if (Auth::guard('admin')->user()->can('admins.delete')) {
         //     $admin = Admin::find($id)->delete();
         //     return redirect('/admin/manager')->withFlashMessage('Manager Deleted');
         // }
         $news = Image::find($id);
           @unLink(base_path().'/public/uplodes/newsphoto/'.$news->first()->name);
           @unLink(base_path().'/public/uplodes/newsphoto70/'.$news->first()->name);
           $news->delete();
        return redirect('/admin/image')->withFlashMessage('Learn Deleted');

     }

}
