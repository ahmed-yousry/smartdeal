<?php

namespace App\Http\Controllers\AdminController;
use App\Http\Controllers\Controller;
use App\signal;
use App\language;
use App\category;
use App\takeprofit;
use Helper;
use Illuminate\Http\Request;


class SignalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


     public function index()
     {
         $signals = signal::with('gettakeprofit')->get();
         //dd($signals);
       return view('admin.signal.index',compact('signals'));
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create()
     {
         // if (Auth::guard('admin')->user()->can('admins.create')) {
         //     $roles = Role::all();
         //     return view('admin.manager.create',compact('roles'));
         // }
         // return redirect()->back();
               // $signals = signal::all();
               // $languages = language::all();
                $categorys = category::all();

              return view('admin.signal.create',compact('categorys'));

     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(Request $request)
     {

       //takeprofit
       $news =  new  signal();
       $news->currancryname = $request->currancryname;
       $news->enterpoint = $request->enterpoint;
       $news->stoplosing = $request->stoplosing;
       $news->image = Helper::uploadImage($request->file('image'),'uplodes/newsphoto/');
       // $news->category = $request->category;
       $news->save();
       $id = $news->id;

       $numberOfData = count($request->takeprofit_id);
       for ($i=0; $i <$numberOfData; $i++) {
         $takeprofit = new takeprofit();
         $takeprofit->signal_id = $id;
         $takeprofit->name = $request->takeprofit_id[$i];
         $takeprofit->save();
       }





         return response()->json(['done']);

     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id)
     {
         //
         // dd($id);
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id)
     {
         // if (Auth::guard('admin')->user()->can('admins.update')) {
         //     $manager = Admin::find($id);
         //     $roles = Role::all();
         //     return view('admin.manager.edit',compact('roles','manager'));
         // }
         // return redirect()->back();

        $categorys = category::all();
         $signal = signal::where('id','=',$id)->with('gettakeprofit')->get();

          //dd($news);
         return view('admin.signal.edit',compact('signal','categorys'));


     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request, $id)
     {
         // $this->validate($request,[
         //     'username' => 'required|string|max:255',
         //     'email' => 'required|string|max:255',
         // ]);
         // $manager = news::find($id)->update($request->except('_token','_method'));
         // return redirect('/admin/news')->withFlashMessage('News Edited');





         $news = signal::find($id);
         $news->currancryname = $request->currancryname;
         $news->enterpoint = $request->enterpoint;
         $news->stoplosing = $request->stoplosing;

         if($file = $request->file('image')){
             @unLink(base_path().'/public/uplodes/newsphoto/'.$news->image);
             $news->image = Helper::uploadImage($request->file('image'),'uplodes/newsphoto/');
         }else{
             $news->image = $news->image;
         }

         $news->save();




 //$numberOfData = $news->gettakeprofit->count();

          $numberOfData = count($request->takeprofit_id);
         if ($numberOfData > 1 ) {
           // code...

         for ($i=0; $i <$numberOfData; $i++) {
           $takeprofit = takeprofit::where('signal_id','=',$id)->get();
           $takeprofit->signal_id = $id;
           $takeprofit->name = $request->takeprofit_id[$i];
           $takeprofit->update();
         }
}


           return response()->json(['done']);

     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id)
     {
         // if (Auth::guard('admin')->user()->can('admins.delete')) {
         //     $admin = Admin::find($id)->delete();
         //     return redirect('/admin/manager')->withFlashMessage('Manager Deleted');
         // }
         $news = news::find($id);
           @unLink(base_path().'/public/uplodes/newsphoto/'.$news->first()->image);
           @unLink(base_path().'/public/uplodes/newsphoto70/'.$news->first()->image);
           $news->delete();
        return redirect('/admin/news')->withFlashMessage('News Deleted');

     }



}
