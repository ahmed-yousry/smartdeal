<?php

namespace App\Http\Controllers\AdminController;

use App\learncategory;
use App\language;

use App\courses;
use Helper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $Gallerycategorys =  courses::with('get_language','get_categories')->get();
      //  dd($Gallerycategorys);

      return view('admin.Courses.index',compact('Gallerycategorys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $languages = language::all();
        $MenuCategorys = learncategory::all();

        return view('admin.Courses.create',compact('languages','MenuCategorys'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $courses = new courses();


       $courses->name = $request->name;
       $date = Carbon::now();
       // set some things
       $courses->years = $date->year;
       $courses->months = $date->month;
       $courses->todays = $date->day;

       // $courses->learncategories_id = $request->learncategories_id;
       $width= "749";
       $hight ="565";
       $courses->image = Helper::uploadImageWithSize($width,$hight,$request->file('image'),'uplodes/newsphoto/');
       $courses->lang_id = $request->language;

       $courses->save();
         return redirect('/admin/courses')->withFlashMessage('courses category has been Stored');
    }


    public function edit($id)
    {
        //
        $Gallerycategorys = courses::where('id','=',$id)->with('get_categories')->get();
        //dd($Gallerycategorys);
        $languages = language::all();

        $MenuCategorys = learncategory::all();

        // dd($news);
        return view('admin.Courses.edit',compact('Gallerycategorys','MenuCategorys','languages'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\learncategory  $learncategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request , $id)
    {
        //
        $courses= courses::find($id);

        $courses->name = $request->name;
        $courses->lang_id = $request->language;
        // $courses->learncategories_id = $request->learncategories_id;
        $width= "749";
        $hight ="565";

        if($file = $request->file('image')){
            @unLink(base_path().'/public/uplodes/newsphoto/'.$courses->image);
            $courses->image = Helper::uploadImageWithSize($width,$hight,$request->file('image'),'uplodes/newsphoto/');
        }else{
            $courses->image = $courses->image;
        }



        $courses->save();

        return redirect('/admin/courses')->withFlashMessage('learncategory category Edited !!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\learncategory  $learncategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $courses = courses::find($id);
        $courses->delete();
       return redirect('/admin/courses')->withFlashMessage('learncategory Deleted');
    }
}
