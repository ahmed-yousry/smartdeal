<?php

namespace App\Http\Controllers;

use App\Providers\AppServiceProvider;
use Illuminate\Http\Request;
use Redirect;

use App\category;
use App\language;

use App\news;


class AllNewsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // protected $test;

    public function __construct()
    {
       // $this->test = $test;

         // $this->middleware('auth');

                //$this->middleware(['auth','verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {



$lang = \Mcamara\LaravelLocalization\Facades\LaravelLocalization::getCurrentLocale();
@$get_lang = language::where('langcode','=',$lang)->get();
@$get_lang_id = $get_lang->first()->id;


@$get_category_news = category::where('lang_id','=',$get_lang_id)->get();
// dd($get_category_news);

$title = "all  news";
$get_news = news::where('lang_id','=',$get_lang->first()->id)
->with('get_categories')
->orderBy('created_at', 'desc')
->take(20)
->get();

 //dd($get_news);


      return view('website.pages-front.news.allnews',compact('title','get_news','get_category_news'));


    }
}
