<?php

namespace App\Http\Controllers;

use App\Providers\AppServiceProvider;
use Illuminate\Http\Request;
use Redirect;

use App\CategoriesTechicalAnalysis;

use App\news_techical_analyses;
use App\language;

class AllTechicalAnalysisController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // protected $test;

    public function __construct()
    {
       // $this->test = $test;

         // $this->middleware('auth');

                //$this->middleware(['auth','verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {



$lang = \Mcamara\LaravelLocalization\Facades\LaravelLocalization::getCurrentLocale();
@$get_lang = language::where('langcode','=',$lang)->get();
@$get_lang_id = $get_lang->first()->id;


@$get_category_news = CategoriesTechicalAnalysis::where('lang_id','=',$get_lang_id)->get();
// dd($get_category_news);

$title = "all  news";
$get_news = news_techical_analyses::where('lang_id','=',$get_lang->first()->id)
->with('get_categories')
->orderBy('created_at', 'desc')
->take(20)
->get();

 //dd($get_news);


      return view('website.pages-front.TechicalAnalysis.AllTechicalAnalysis',compact('title','get_news','get_category_news'));


    }
}
