<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\learn;
use App\language;
use App\learncategory;
use App\courses;

class LearnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($menuCategory)
    {
        //

        $lang = \Mcamara\LaravelLocalization\Facades\LaravelLocalization::getCurrentLocale();
        @$get_lang = language::where('langcode','=',$lang)->get();
        @$get_lang_id = $get_lang->first()->id;


          $learns =  learncategory::where('lang_id','=',$get_lang_id)
          ->where('menuCategory_id','=',$menuCategory)
          ->with('get_courses.get_lesson')
          ->get();

          //dd($learns);
          @$title = $learns->first()->name;

         return view('website.pages-front.Learn.index',compact('learns','title'));
    }


    public  function  CoursesDetails($id)
    {
      $lang = \Mcamara\LaravelLocalization\Facades\LaravelLocalization::getCurrentLocale();
      @$get_lang = language::where('langcode','=',$lang)->get();
      @$get_lang_id = $get_lang->first()->id;


        @$learns =  learn::where('id','=',$id)
        ->with('get_courses.get_lesson')
        ->get();


         return response()->json(['data' => $learns]);

          //dd($learns);
        @$title = $learns->first()->title;


        return view('website.pages-front.Learn.Details',compact('title','learns'));

    }



    public  function  CoursesDetailsFromAll($id)
    {

      $lang = \Mcamara\LaravelLocalization\Facades\LaravelLocalization::getCurrentLocale();
      @$get_lang = language::where('langcode','=',$lang)->get();
      @$get_lang_id = $get_lang->first()->id;


        $learns =   courses::where('lang_id','=',$get_lang_id)
        ->where('id','=',$id)
        ->with('get_lesson')
        ->get();

        //dd($learns);
        @$title = $learns->first()->name;

       return view('website.pages-front.Learn.CoursesDetailsFromAll',compact('learns','title'));

    }


}
