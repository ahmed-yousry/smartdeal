<?php

namespace App\Http\Controllers;

use App\Providers\AppServiceProvider;
use Illuminate\Http\Request;
use Redirect;

use App\category;
use App\language;
use App\CategoriesTechicalAnalysis;
use App\our_traders_news_TechicalAnalysis;
use App\User;
use App\story;
use App\report;
use App\news;
use App\Slider;
use App\Gallery;
use App\Gallerycategory;
use App\note;
use App\news_techical_analyses;

class IndexController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // protected $test;

    public function __construct()
    {
       // $this->test = $test;

         // $this->middleware('auth');

                //$this->middleware(['auth','verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {



$lang = \Mcamara\LaravelLocalization\Facades\LaravelLocalization::getCurrentLocale();
@$get_lang = language::where('langcode','=',$lang)->get();
@$get_lang_id = $get_lang->first()->id;
@$get_category_news = category::where('lang_id','=',$get_lang_id)->get();
 @$get_category_TechicalAnalysis = CategoriesTechicalAnalysis::where('lang_id','=',$get_lang_id)->get();

  // @$get_OurTraders_have_techical_analyses = our_traders_news_TechicalAnalysis::with('get_user')->get();
$get_OurTraders_have_techical_analyses = User::has('get_news_TechicalAnalysis')->get();
// $users = User::with('get_news_TechicalAnalysis')->get()->sortByDesc('latestPost.created_at');
$get_storys = story::where('lang_id','=',$get_lang_id)
->orderBy('id', 'desc')
->take(6)->get();

// $news_techical_analyses = news_techical_analyses::where('lang_id','=',$get_lang_id)
// ->orderBy('id', 'asc')
// ->take(4)->get();

$news_techical_analyses = news_techical_analyses::where('lang_id','=',$get_lang_id)
->with('get_categories')
->orderBy('created_at', 'desc')
->take(4)
->get();



$get_report = report::where('lang_id','=',$get_lang_id)->orderBy('id', 'DESC')->get()->first();

$get_sliders = Slider::where('lang_id','=',$get_lang_id)->get();
$get_note = note::where('lang_id','=',$get_lang_id)
->orderBy('id', 'desc')
->get();


$get_Gallerycategorys = Gallerycategory::where('lang_id','=',$get_lang_id)->get();

$get_Gallerys = Gallery::with('get_categories')->where('lang_id','=',$get_lang_id)
->orderBy('created_at', 'desc')
->take(8)
->get();


$get_news = news::where('lang_id','=',$get_lang->first()->id)
->orderBy('created_at', 'desc')
->take(6)
->get();

$get_ourTraders_have_techical_analyses = our_traders_news_TechicalAnalysis::where('lang_id','=',$get_lang->first()->id)
->orderBy('created_at', 'desc')
->take(6)
->get();

$chunks = collect($news_techical_analyses)->chunk(4);

@$count_chunks = count($chunks);
@$first_section = count($chunks[0]);
@$secound_section = count($chunks[1]);
    //dd($chunks);
$get_ourTraders_have_techical_analyses_active = $get_ourTraders_have_techical_analyses->first()->id;





      return view('website.pages-front.index',compact('first_section','secound_section','chunks','count_chunks','news_techical_analyses','get_note','get_Gallerycategorys','get_Gallerys','get_ourTraders_have_techical_analyses_active','get_ourTraders_have_techical_analyses','get_sliders','get_report','get_news','get_storys','get_OurTraders_have_techical_analyses','get_category_news','get_category_TechicalAnalysis'));


    }
}
