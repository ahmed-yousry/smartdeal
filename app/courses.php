<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class courses extends Model
{
    //

    protected $table='courses';
    protected $fillable = [
        'lang_id','image','name','image','years','months','todays',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

     public function get_language(){
         return $this->belongsTo('App\language','lang_id','id');
     }

    public function get_categories(){
        return $this->belongsTo('App\learncategory','learncategories_id','id');
    }


    public function get_lesson(){
        return $this->hasMany('App\learn','categories_id','id');
    }

}
