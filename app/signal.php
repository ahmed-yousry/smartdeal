<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class signal extends Model
{
    //

    protected $table='signals';
    protected $fillable = [
        'currancryname', 'enterpoint','stoplosing'
    ];



    // public function gettakeprofit(){
    //     return $this->belongsTo('App\takeprofit','signal_id','id');
    //
    // }


    public function gettakeprofit(){
        return $this->hasMany('App\takeprofit','signal_id','id');
    }

}
