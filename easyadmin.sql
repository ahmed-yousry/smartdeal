-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 15, 2020 at 09:09 AM
-- Server version: 8.0.22-0ubuntu0.20.04.3
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smartl24_website`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int UNSIGNED NOT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `password`, `email`, `name`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'manager', '$2y$10$Yw9e8Nc/1gajUfm6MXXLOutInuUMNdfj0Y/9fvRfxaMBKFkEA2k2e', 'manager@manager.com', 'manager', 'foXAjxOsqN3E7wocau6uEYOY9Ecla7puVZdFjU1IMbhHrz3bxwFPVRv4p1MX', '2020-11-25 16:08:37', '2020-11-25 16:08:37'),
(2, 'admin', '$2y$10$IWpK5.mr7ol4WUNAxrXF2O1xBpZc3Yycxqg9ve4UMgb..KXqSjqxG', 'admin@admin.com', 'admin', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_role`
--

CREATE TABLE `admin_role` (
  `id` int UNSIGNED NOT NULL,
  `admin_id` int UNSIGNED NOT NULL,
  `role_id` int UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_role`
--

INSERT INTO `admin_role` (`id`, `admin_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `showname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_id` int UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `showname`, `lang_id`, `created_at`, `updated_at`) VALUES
(1, 'forex', 'forex', 1, NULL, NULL),
(2, 'stock', 'stock', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories_techical_analyses`
--

CREATE TABLE `categories_techical_analyses` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `showname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_id` int UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories_techical_analyses`
--

INSERT INTO `categories_techical_analyses` (`id`, `name`, `showname`, `lang_id`, `created_at`, `updated_at`) VALUES
(1, 'forex', 'forex', 1, NULL, NULL),
(2, 'فوريكس', 'فوريكس', 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_id` int UNSIGNED NOT NULL,
  `categorie_id` int UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `name`, `image`, `lang_id`, `categorie_id`, `created_at`, `updated_at`) VALUES
(1, 'عربى صوره', '1606737735.jpg', 2, 1, '2020-11-30 17:02:15', '2020-11-30 17:03:17'),
(3, 'مهندس عبلى', '1606739112.jpg', 2, 4, '2020-11-30 17:25:12', '2020-11-30 17:25:12'),
(4, 'englsih  image', '1606739253.jpg', 1, 5, '2020-11-30 17:27:33', '2020-11-30 17:27:33'),
(5, 'englsih', '1606739514.jpeg', 1, 3, '2020-11-30 17:31:54', '2020-11-30 17:31:54');

-- --------------------------------------------------------

--
-- Table structure for table `gallerycategories`
--

CREATE TABLE `gallerycategories` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_id` int UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gallerycategories`
--

INSERT INTO `gallerycategories` (`id`, `name`, `lang_id`, `created_at`, `updated_at`) VALUES
(1, 'عربى', 2, '2020-11-30 16:58:36', '2020-11-30 16:58:36'),
(3, 'english', 1, '2020-11-30 17:05:35', '2020-11-30 17:05:35'),
(4, 'شرم', 2, '2020-11-30 17:24:27', '2020-11-30 17:24:27'),
(5, 'english_image', 1, '2020-11-30 17:27:06', '2020-11-30 17:27:06');

-- --------------------------------------------------------

--
-- Table structure for table `image_news`
--

CREATE TABLE `image_news` (
  `id` int UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `queue` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint UNSIGNED NOT NULL,
  `reserved_at` int UNSIGNED DEFAULT NULL,
  `available_at` int UNSIGNED NOT NULL,
  `created_at` int UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `langcode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `langcode`, `created_at`, `updated_at`) VALUES
(1, 'english', 'en', NULL, NULL),
(2, 'arabic ', 'ar', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `learncategories`
--

CREATE TABLE `learncategories` (
  `id` int UNSIGNED NOT NULL,
  `lang_id` int UNSIGNED NOT NULL,
  `menuCategory_id` int UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `learncategories`
--

INSERT INTO `learncategories` (`id`, `lang_id`, `menuCategory_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'harmonic  stratige', NULL, NULL),
(2, 1, 1, 'aleuite', '2020-12-06 14:23:38', '2020-12-06 14:23:38'),
(3, 1, 2, 'usd/euro  analsis', '2020-12-06 14:47:15', '2020-12-06 14:47:15'),
(4, 2, 3, 'هارمونيك كورس', '2020-12-06 14:51:59', '2020-12-06 14:51:59');

-- --------------------------------------------------------

--
-- Table structure for table `learns`
--

CREATE TABLE `learns` (
  `id` int UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_id` int UNSIGNED NOT NULL,
  `categories_id` int UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `learns`
--

INSERT INTO `learns` (`id`, `title`, `image`, `lang_id`, `categories_id`, `description`, `body`, `created_at`, `updated_at`) VALUES
(1, 'first liesson harmonic', '1607250478.jpg', 1, 1, 'aesdrwer', '<p><img src=\"https://xdsoft.net/jodit/files/1966051_524428741092238_1051008806888563137_o.jpg\" style=\"width: 300px;\"></p><p><br></p><p>wwerwerrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr</p><p>ssssssssssssssssssssssssssssssss</p><p>s</p><p><br></p><p>ssss</p><p>s</p><p>s</p><p>s</p><p><br></p>', '2020-12-06 14:39:56', '2020-12-06 15:27:58'),
(2, 'first liesson  aleuite', '1607250490.jpg', 1, 2, 'aleuite', '<p>aleuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuite</p><p>aleuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuite</p><p>aleuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuitealeuite<br></p>', '2020-12-06 14:43:58', '2020-12-06 15:28:11'),
(3, 'first image in  usd/uro', '1607250500.jpg', 1, 3, 'asdefwserfwer', '<p>sdfsdfsdfsdfsdfs</p>', '2020-12-06 14:47:50', '2020-12-06 15:28:20'),
(4, 'اول  درس فى  الهارمونك', '1607250514.jpg', 2, 4, 'هعارمونك', '<p>هارمونيك&nbsp;&nbsp; </p><p>هارمونيك&nbsp;&nbsp; </p><p>هارمونيك&nbsp;&nbsp; </p><p><br></p><p>هارمونيك&nbsp;&nbsp; </p><p>هارمونيك&nbsp;&nbsp; </p><p>هارمونيك&nbsp;&nbsp; </p><p>هارمونيك&nbsp;&nbsp; </p><p><br></p><p>هارمونيك&nbsp;&nbsp; </p><p>هارمونيك&nbsp;&nbsp; </p><p>هارمونيك&nbsp;&nbsp; </p><p>هارمونيك&nbsp;&nbsp; </p><p><br></p><p>هارمونيك&nbsp;&nbsp; <br></p>', '2020-12-06 14:52:35', '2020-12-06 15:28:34'),
(5, 'yousry  title', '1607251042.jpeg', 1, 2, 'fsdfsdfsdf', '<p>sdfsdddddddddddddddddddddddddddddddddddddddddd</p>', '2020-12-06 15:37:22', '2020-12-06 15:37:22'),
(6, 'adsfs', '1607612220.jpg', 1, 1, 'werwerwerwer', '<p><br></p><p><br></p>', '2020-12-10 19:57:01', '2020-12-10 19:57:01'),
(7, 'werwer', '1607612269.jpg', 1, 1, 'edrygtert', '<p>56 7yrtyrtyrty<br></p>', '2020-12-10 19:57:49', '2020-12-10 19:57:49');

-- --------------------------------------------------------

--
-- Table structure for table `menu_categories`
--

CREATE TABLE `menu_categories` (
  `id` int UNSIGNED NOT NULL,
  `lang_id` int UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_categories`
--

INSERT INTO `menu_categories` (`id`, `lang_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'stratige', NULL, NULL),
(2, 1, 'couers forex', '2020-12-06 14:46:23', '2020-12-06 14:46:49'),
(3, 2, 'هارمونيك', '2020-12-06 14:51:29', '2020-12-06 14:51:29');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000001_type', 1),
(2, '2014_10_12_000002_create_users_table', 1),
(3, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2018_02_07_142058_create_admins_table', 1),
(6, '2018_02_26_100156_create_settings_table', 1),
(7, '2018_07_15_152441_create_jobs_table', 1),
(8, '2018_09_05_085743_create_roles_table', 1),
(9, '2018_09_05_085825_create_permissions_table', 1),
(10, '2018_09_17_145222_create_providers_table', 1),
(11, '2019_01_25_1552040_create_admin_role', 1),
(12, '2019_07_29_142710_create_user_diractions_table', 1),
(13, '2019_08_01_095342_create_u_roles_table', 1),
(14, '2019_08_03_094940_create_user_roles_table', 1),
(15, '2019_09_04_152557_create_verify_users_table', 1),
(16, '2020_11_11_112200_create_languages_table', 1),
(17, '2020_11_11_112201_create_categories_table', 1),
(18, '2020_11_11_112403_create_image_news_table', 1),
(19, '2020_11_11_112404_create_news_table', 1),
(20, '2020_11_11_125305_create_categories_techical_analyses_table', 1),
(21, '2020_11_13_125314_create_news_techical_analyses_table', 1),
(22, '2020_11_17_143002_create_our_traders_news_table', 1),
(23, '2020_11_18_090551_create_our_traders_news__techical_analyses_table', 1),
(24, '2020_11_26_120307_create_teams_table', 2),
(25, '2020_11_27_084403_create_stories_table', 3),
(26, '2020_11_29_081746_create_reports_table', 4),
(27, '2018_02_19_171937_create_sliders_table', 5),
(30, '2020_11_30_095202_create_gallerycategories_table', 6),
(31, '2020_11_30_095222_create_galleries_table', 6),
(37, '2020_12_01_092624_create_menu_categories_table', 9),
(38, '2020_12_01_092625_create_learncategories_table', 9),
(39, '2020_12_01_093056_create_learns_table', 9),
(40, '2020_12_03_110923_create_notes_table', 10);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int UNSIGNED NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_id` int UNSIGNED NOT NULL,
  `categories_id` int UNSIGNED NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `image`, `title`, `lang_id`, `categories_id`, `description`, `body`, `status`, `created_at`, `updated_at`) VALUES
(1, '1606306359.jpg', 'asdasdasd', 1, 1, 'asdasasdasdasddasd', '<p>asdasdasdasdasdasdasd</p>', 'active', '2020-11-25 17:12:39', '2020-11-25 17:12:39'),
(2, '1606310003.jpg', 'What is DAO', 1, 1, 'Imagine\r\n a vending machine that not only takes money from you and gives you a \r\nsnack in return but also uses that money to automatically re-order the \r\ngoods. This machine also orders cleaning services and pays its rent all \r\nby itself. Moreover, as you put money into that machine, you and its \r\nother users have a say in what snacks it will order and how often should\r\n it be cleaned. It has no managers, all of those processes were \r\npre-written into code.', '<div class=\"guide-content\"><div class=\"guide-content-title\"><h1>\r\n    What is DAO\r\n  </h1></div> <div class=\"guide-content-poster\"><img srcset=\"https://images.cointelegraph.com/images/740_aHR0cHM6Ly9zMy5jb2ludGVsZWdyYXBoLmNvbS9zdG9yYWdlL3VwbG9hZHMvdmlldy9mZjQ3NWIyZjRmOGMxNjg2MWRlNTg5ZDg5NTJhNGJiOS5qcGc=.jpg 1x, https://images.cointelegraph.com/images/1480_aHR0cHM6Ly9zMy5jb2ludGVsZWdyYXBoLmNvbS9zdG9yYWdlL3VwbG9hZHMvdmlldy9mZjQ3NWIyZjRmOGMxNjg2MWRlNTg5ZDg5NTJhNGJiOS5qcGc=.jpg 2x\" src=\"https://images.cointelegraph.com/images/740_aHR0cHM6Ly9zMy5jb2ludGVsZWdyYXBoLmNvbS9zdG9yYWdlL3VwbG9hZHMvdmlldy9mZjQ3NWIyZjRmOGMxNjg2MWRlNTg5ZDg5NTJhNGJiOS5qcGc=.jpg\" alt=\"In DAO managers are no longer needed\"></div> <div class=\"guide-content-text\"><p dir=\"ltr\">Imagine\r\n a vending machine that not only takes money from you and gives you a \r\nsnack in return but also uses that money to automatically re-order the \r\ngoods. This machine also orders cleaning services and pays its rent all \r\nby itself. Moreover, as you put money into that machine, you and its \r\nother users have a say in what snacks it will order and how often should\r\n it be cleaned. It has no managers, all of those processes were \r\npre-written into code.</p> <p dir=\"ltr\"><img alt=\"DAO (decentralized autonomous organization)\" src=\"https://lh4.googleusercontent.com/FByZXv-G4DKPZ2DBN7Js12C3l6CTDab69L_TYt5c1N-BntTcwbm4ahpLb-XLdblnTowsH-wqSJEpCbt4pFLqxTe2F6gJba9h-spOMy9JPz285j5CsFJLbKUAaDybpCIukFZ2QKfd\" title=\"DAO (decentralized autonomous organization)\" width=\"1500\" height=\"1000\"></p> <p dir=\"ltr\">This\r\n is, roughly, how a DAO or a Decentralized Autonomous Organization, \r\nworks. The idea of such management model has been circulating in the \r\ncryptocurrency community ever since Bitcoin managed to get rid of \r\nmiddlemen in financial transactions. Similarly, the main idea behind DAO\r\n is establishing a company or an organization that can fully function \r\nwithout hierarchical management.</p> <p>It is essential to draw a \r\ndistinction between DAO as a type of organizations and The DAO, which is\r\n merely a name of one of such organizations. The project was one of the \r\nfirst attempts at creating a DAO and it failed spectacularly within due \r\nto a mistake in its initial code.</p> <h2 id=\"contentRef_0\">How DAOs work</h2> <p>Initially,\r\n Bitcoin was considered to be the first ever fully-functional DAO, as it\r\n has a pre-programmed set of rules, functions autonomously and is \r\ncoordinated through a distributed consensus protocol. Since then, the \r\nuse of smart contracts was enabled on the Ethereum platform, which \r\nbrought the creation of DAOs closer to the general public and shaped \r\ntheir current look.</p> <p>But what does a DAO need to be fully \r\noperational? First of all, a set of rules according to which it will \r\noperate. Those rules are encoded as a smart contract, which is \r\nessentially a computer program, that autonomously exists on the \r\nInternet, but at the same time it needs people to perform task that it \r\ncan’t do by itself.</p> <p>Once the rules are established, a DAO enters a\r\n funding phase. This is a very important part for two reasons. Firstly, a\r\n DAO has to have some kind of an internal property, tokens that can be \r\nspent by the organization or used to reward certain activities within \r\nit. Secondly, by investing in a DAO, users get voting rights and \r\nsubsequently the ability to influence the way it operates.</p> <p><img alt=\"Tokens on a factory line\" src=\"https://lh4.googleusercontent.com/0cyIzmUdPVSPpKHEeg78R2rqtfPbqyQgeZYfplpXySys4ZZ2bQda0G4oO3KozvAh0oY1nlAXpJWVys78cvGTgPeOHMn3t8J4ZFmtqaEClmRSFFZ4y3f6v-icXNJ_1BNVti3O_G0a\" title=\"Tokens on a factory line\" width=\"725\" height=\"483\"></p> <p dir=\"ltr\">After\r\n the funding period is over and a DAO is deployed, it becomes fully \r\nautonomous and completely independent from its creators as well as \r\nanyone else for that matter. They’re open source, which means their code\r\n can be viewed by anyone. Moreover, all of the rules and financial \r\ntransactions are recorded in the Blockchain. This makes DAOs fully \r\ntransparent, immutable and incorruptible.</p> <p>Once a DAO is \r\noperational, all the decisions on where and how to spend its funds are \r\nmade via reaching a consensus. Everyone who bought a stake in a DAO can \r\nmake proposals regarding its future. In order to prevent the network \r\nbeing spammed with proposals, a monetary deposit could be required to \r\nmake one.</p> <p>Subsequently, the stakeholders vote on the proposal. In\r\n order to perform any action, the majority needs to agree on doing so. \r\nThe percentage required to reach that majority can vary depending on a \r\nDAO, as it can be specified in its code.</p> <p>Essentially, DAOs enable\r\n people to exchange its funds with anyone in the world. This can be done\r\n in the form of an investment, a charitable donation, money raising, \r\nborrowing and so on, all without an intermediary. One potentially major \r\nproblem with the voting system is that even if a security hole was \r\nspotted in an initial code, it can’t be corrected until the majority \r\nvotes on it. While the voting takes place, said hackers can exploit a \r\nbug in the code.</p> <blockquote> <p>Finally, it is important to note \r\nthat a DAO isn’t capable of building a product, writing a code or \r\ndeveloping a piece of hardware. Instead, a contractor can be hired to \r\nperform a required task. This appointment is done through the same \r\nvoting process, while a smart contract will ensure a swift payment upon \r\nthe correct completion of the task.</p> </blockquote> <div class=\"embed-responsive embed-responsive-16by9\"> <center> <p><jodit data-jodit-temp=\"1\" draggable=\"true\" data-jodit_iframe_wrapper=\"1\" style=\"display: block; width: 1310px; height: 737px;\" contenteditable=\"false\"><iframe allowfullscreen=\"\" src=\"https://www.youtube.com/embed/Pyi8-qm02hs\" width=\"560\" height=\"315\" frameborder=\"0\"><br></iframe></jodit></p> </center> </div> <h2 id=\"contentRef_1\">How to become DAO shareholder and why</h2> <p>Investing in a DAO is relatively easy, especially if you know how to buy Ether or <a href=\"https://cointelegraph.com/bitcoin-for-beginners/how-can-i-buy-bitcoins\" target=\"_blank\">Bitcoin</a>\r\n and already have a wallet. All you really need to do is buy tokens of a\r\n particular DAO, which is roughly equivalent to buying shares of a \r\ncompany.</p> <p>Once the funding phase is closed, you will be able to \r\nmake proposals as well as vote on them and perhaps even make a profit. \r\nThe amount of purchase tokens will correlate with the amount of voting \r\npower you will get.</p> <p>Before you invest, though, be sure to know \r\nexactly what you’re getting yourself into. DAOs are completely \r\ntransparent and their underlying codes are always open-source, which \r\nmeans you have a chance of reviewing said codes in order to be sure that\r\n there are no bug or mistakes in it.</p> <p dir=\"ltr\"><img alt=\"Voting process in DAO\" src=\"https://lh3.googleusercontent.com/-I2UX1D9kopzd7Re3-vrxZL2WqbAlfmUg1_r90g38iQJHNIgl8mspgMcMi7Kqq24cP1b-QxcXeqEFUPsAoMpA3sRO_9-eCbCdwDUBIBt44wOOVbrBfvSGketLSBMVhxRgPl3aB2f\" title=\"Voting process in DAO\" width=\"725\" height=\"483\"></p> <h2 dir=\"ltr\" id=\"contentRef_2\">What is Dapp</h2> <p>Dapps,\r\n or Decentralized Applications, are essentially unstoppable apps, which \r\nwork on the Ethereum Blockchain and are powered by smart contracts. The \r\nmain difference from ordinary apps is that Dapps are fully autonomous, \r\nthey don’t require a middleman to operate and basically immune to \r\ncensorship. In other words, they establish a direct connection between a\r\n user and a service. Through that, users can fully control the \r\ninformation and data they share.</p> <p>DAOs are basically a very \r\nambitious breed of decentralized apps. As described in the Ethereum \r\nwhite paper, they fall into ‘other’ category, which includes voting and \r\ngovernance systems. The two other types of Dapps are money managing \r\napplications and apps where money is involved, but they also require \r\nanother piece (insurance, charity, property, etc.).</p> <p dir=\"ltr\"><img alt=\"Blockchain as a game\" src=\"https://lh6.googleusercontent.com/XVzcMlUJX41HXr-i7VLatIdjLITEKbKVjErl_vx8IFU9dB6Nw4ke6OBkyjM_1GfH-7kcGtUkQ1AH5NiOUSfkF4coHmegizKOJdxTkIILy96eSeTukI7utQ63MjXDTAG0AygFUIeN\" title=\"Blockchain as a game\" width=\"1500\" height=\"1000\"></p> <h2 dir=\"ltr\" id=\"contentRef_3\">Story of The DAO</h2> <p>‘The DAO’ is a name of one particular DAO, which was created by a team behind a German startup <a href=\"http://slock.it\">slock.it</a>\r\n which specialized in ‘smart locks’ that let people share their property\r\n in a decentralized version of Airbnb. The DAO was deployed in May 2016,\r\n when it was funded via a token sale. Somehow, the project managed to \r\nbecome the most successful crowdfunding campaign in history, having \r\nraised over $150 mln.</p> <div class=\"row\"> <div class=\"col-lg-6 col-md-6 col-sm-12 col-sm-12\"><img alt=\"The DAO logo\" src=\"https://lh5.googleusercontent.com/lPfPjRQIAKImyvfFzpEECaRJKJTq_i-iKbhGz391MR48KvRqyxmmCsma3DvHDN3geSrDzC4oDQ1E34MWjcKjGZX3oh0hnNzppXs4iB3NUCrSmSD-OyJLPHCirRl4CZ83lqcRuH8A\" title=\"The DAO logo\" width=\"500\" height=\"500\"></div> <div class=\"col-lg-6 col-md-6 col-sm-12 col-sm-12\">&nbsp;</div> </div> <p>The\r\n DAO’s code wasn’t perfect, and as it was open-source and available to \r\nview for everyone, someone found a bug to exploit. So, on June 17th an \r\nanonymous hacker or a group of hackers, started siphoning money from The\r\n DAO into a ‘child DAO,’ which copied The DAO’s structure. Before the \r\ndraining of funds was stopped, the hacker managed to steal more than $50\r\n mln worth of Ether.</p> <p dir=\"ltr\">Despite the fact that a bug in The\r\n DAO’s code was exploited to steal the funds, the hack seriously \r\nundermined both Ethereum’s reputation as a hosting platform and the very\r\n concept of DAOs. Moreover, it caused the Ethereum network to split in \r\ntwo. However, it is important to realize that all of this could’ve been \r\navoided by additionally testing the code. Perhaps, this hack was an \r\nimportant milestone in the developing history of DAOs which showed the \r\npotential weaknesses and, undoubtedly, such weaknesses will be taken \r\ninto consideration by future DAOs.</p> <p dir=\"ltr\"><img alt=\"The DAO logo and treasures\" src=\"https://lh3.googleusercontent.com/W_XNWZsFvBWgGJlIVYFzEJusQ-5gCTbZ6ueNmYgdnEXe6ZyhRf9AD0fYllUbM4KJWZAoytFa9B7VFcCyB72vNaRBO5y2SKdeT9isF6ZcAcCcv0PdUgHXl4SDxO-UTCmnh1AIO4MA\" title=\"The DAO logo and treasures\" width=\"725\" height=\"483\"></p> <h2 dir=\"ltr\" id=\"contentRef_4\">Advantages of DAOs</h2> <p>Undeniably,\r\n the very concept of DAOs is extremely exciting, as it strives to solve \r\neverything that’s wrong with how modern-day organizations are run. A \r\nperfectly structured DAO gives every investor an opportunity to shape \r\nthe organization. There’s no hierarchical structure, which means every \r\ninnovative idea can be put forward by anyone and considered by the \r\nentire organization. A set of pre-written rules that every investor is \r\naware of before joining the organization as well as the voting system \r\nleave no room for quarreling whatsoever.</p> <p>Moreover, as both \r\nputting a proposal forward and voting for it requires an investor to \r\nspend a certain amount of money, it pushes them to evaluate their \r\ndecisions and not waste time on ineffective solutions. Finally, as all \r\nthe rules, as well as every single financial transaction, are recorded \r\nin the Blockchain, available for review to anyone, DAOs are completely \r\ntransparent. Everyone taking part helps decide on how to spend the funds\r\n and they can track how those funds are spent.</p> <p dir=\"ltr\"><img alt=\"Difference of decentralized organization from traditional\" src=\"https://lh5.googleusercontent.com/iZCXDqbMC7YX85odTho-YRg5PMC_mi5fl-dFc-2vdZCl--px5ZkWlYpalgAFNettD3LVH9i_1DViR_U04ewja3Ngn99c3xDV_L0YI7o40AVbUXaaeKqMgXZ_45dFQs5PRlvhUt-G\" title=\"Difference of decentralized organization from traditional\" width=\"1024\" height=\"678\"></p> <h2 dir=\"ltr\" id=\"contentRef_5\">Disadvantages and critique</h2> <p>DAOs,\r\n just like pretty much everything else connected to cryptocurrencies, \r\nare an extremely new and, to some extent, a revolutionary technology. \r\nNaturally, projects like these will attract a lot of criticism. For \r\ninstance, MIT Technology Review <a href=\"https://www.technologyreview.com/s/601480/the-autonomous-corporation-called-the-dao-is-not-a-good-way-to-spend-130-million/\">considers</a>\r\n the idea of entrusting the masses with important financial decisions a \r\nbad one and the one that isn’t likely to yield any returns. In their \r\narticle, they say that much in the world will have to change for the \r\nDAO-related projects to succeed on any scale.</p> <p dir=\"ltr\"><img alt=\"The DAO mathematics\" src=\"https://lh5.googleusercontent.com/CanzYAA2tBY2jTzmUdvHaiE9d63LsfOCkSGibB7ai5Wjxa9IPKpeeaFRf56tINRgLCKvUR8r56geMoOIEi2bOw4fg_sBcctgsHy1BUtAVdOkFYKy3u33CrcsAGybeRgThYI6kFIK\" title=\"The DAO mathematics\" width=\"725\" height=\"483\"></p> <p dir=\"ltr\">Apart\r\n from quite a conservative sentiment that masses can’t be entrusted with\r\n investments, there are other concerns surrounding DAOs. The most urgent\r\n problem, especially after The DAO hack, is a security problem and it is\r\n connected with the ‘unstoppable code’ principle. During the attack, \r\nobservers and investors watched helplessly as the funds were siphoned \r\nout of The DAO, but couldn’t do anything, as the attacker was \r\ntechnically following the rules. Of course, such attacks can be avoided \r\nif the code is well-composed and bug-free.</p> <p>Finally, in order for \r\nstartups that operate as DAOs to be able to conduct business outside of a\r\n Blockchain network and communicate with a physical world of financial \r\ninstruments and intellectual property, there needs to be some kind of a \r\nlegal framework. Legal uncertainty is an issue that has been plaguing \r\nthe world of cryptocurrencies due to the technology within it being so \r\nnew and radically different, but the solution seems to be just a matter \r\nof time.</p> <h2 id=\"contentRef_6\">Examples of DAO</h2> <p>Essentially, \r\nany autonomous organization with a decentralized governance and \r\nbudgeting system can be called a DAO. This makes practically every \r\ndecentralized cryptocurrency network a DAO, especially considering the \r\ninitial crowdfunding period the precede the official launch. Below is a \r\nshort list of some of the most well-known successful DAOs.</p> <p><a href=\"https://www.dash.org\" rel=\"nofollow\" target=\"_blank\">Dash</a> — An open-source, peer-to-peer cryptocurrency, which offers instant payments and private transactions.</p> <p dir=\"ltr\"><a href=\"https://digix.global\" rel=\"nofollow\" target=\"_blank\">Digix Global</a>\r\n — The gold standard in peer-to-peer digital assets. Each Digix Gold \r\ntoken represents 1 gram of LMBA standard gold and secured in Safehouse \r\nvaults.</p> <p dir=\"ltr\"><a href=\"https://bitshares.org\" rel=\"nofollow\" target=\"_blank\">BitShares</a>\r\n — A decentralized cryptocurrency exchange, which markets itself as a \r\nfast and fluid trading platform, providing the freedom of cryptocurrency\r\n combine with the stability of the dollar.</p> </div></div>', 'active', '2020-11-25 18:13:25', '2020-11-25 18:13:25'),
(3, '1606478004.jpg', 'Dollar Weakens; Stimulus Expected to Combat Pandemic', 2, 1, 'By Peter Nurse\r\n\r\nInvesting.com - The dollar weakened in early European trade Friday, set to post weekly losses with traders expecting large-scale stimulus from the new administration to combat the Covid-19 pandemic.\r\n\r\n\r\nThe European Union chief negotiator Michel Barnier will talk on Friday with some of the bloc\'s ministers responsible for fisheries to discuss the state of play in the trade discussions with Britain, an EU official said.', '<p dir=\"ltr\">By Peter Nurse</p>\r\n<p dir=\"ltr\">Investing.com - The dollar weakened in early European trade\r\n Friday, set to post weekly losses with traders expecting large-scale \r\nstimulus from the new administration to combat the Covid-19 pandemic.</p>\r\n<p dir=\"ltr\">At 3:55 AM ET (0755 GMT), the Dollar Index, which tracks \r\nthe greenback against a basket of six other currencies, was down 0.1% at\r\n 91.892, near the three-month low since late Thursday. Volumes are \r\nlikely to remain limited after Thursday’s Thanksgiving holiday in the \r\nU.S., with many traders set to enjoy a long weekend.</p>\r\n<p dir=\"ltr\"><span class=\"aqPopupWrapper js-hover-me-wrapper\"><a href=\"https://www.investing.com/currencies/eur-usd\" id=\"5fc0e7171dec5\" class=\"aqlink js-hover-me\" hoverme=\"aql\" data-pairid=\"1\">EUR/USD</a></span> climbed 0.1% to 1.1926, close to a two-month high, while <span class=\"aqPopupWrapper js-hover-me-wrapper\"><a href=\"https://www.investing.com/currencies/usd-jpy\" id=\"5fc0e7171e1a1\" class=\"aqlink js-hover-me\" hoverme=\"aql\" data-pairid=\"3\">USD/JPY</a></span> fell 0.2% to 104.06. Additionally, <span class=\"aqPopupWrapper js-hover-me-wrapper\"><a href=\"https://www.investing.com/currencies/aud-usd\" id=\"5fc0e7171e27f\" class=\"aqlink js-hover-me\" hoverme=\"aql\" data-pairid=\"5\">AUD/USD</a></span> rose 0.2% to 0.7372, near a three-month high, while <span class=\"aqPopupWrapper js-hover-me-wrapper\"><a href=\"https://www.investing.com/currencies/nzd-usd\" id=\"5fc0e7171e351\" class=\"aqlink js-hover-me\" hoverme=\"aql\" data-pairid=\"8\">NZD/USD</a></span> gained 0.2% to 0.7021, near its strongest level in over two years.&nbsp;</p>\r\n<p dir=\"ltr\">“The dollar is back at its 2018 lows with DXY [the Dollar \r\nIndex] breaking below the key 92.00 support,” said analysts at ING, in a\r\n research note, stating that the pandemic “appears particularly \r\nconcerning as major U.S. cities are experiencing large spikes in cases \r\nwhich may fuel further speculation that President-elect Joe Biden will \r\nopt for tougher restrictions once he takes office.”</p>\r\n<p dir=\"ltr\">Outgoing President Donald Trump said on Thursday he will \r\nleave office if the Electoral College votes for Biden, the closest he \r\nhas come to conceding the Nov. 3 election and another sign of the \r\nreturning normalcy in U.S. political circles.</p>\r\n<p dir=\"ltr\">Biden has made it clear that tackling the pandemic is his \r\nfirst priority, and has called on U.S. lawmakers to pass a new stimulus \r\nbill before he officially takes office in January. His nomination of \r\nJanet Yellen, former head of the Federal Reserve who is credited with \r\nhelping steer the economic recovery after the 2007 financial crisis and \r\nensuing recession, has also played into the underlying belief that more \r\nfiscal help is on its way, likely pressurising the dollar.</p>\r\n<p dir=\"ltr\"><span class=\"aqPopupWrapper js-hover-me-wrapper\"><a href=\"https://www.investing.com/currencies/gbp-usd\" id=\"5fc0e7171e42d\" class=\"aqlink js-hover-me\" hoverme=\"aql\" data-pairid=\"2\">GBP/USD</a></span>\r\n gained 0.1% to $1.3369, near a three-month high of $1.3399 it touched \r\non Thursday, as market participants look for progress on Brexit talks.</p>\r\n<p dir=\"ltr\">The European Union chief negotiator Michel Barnier will \r\ntalk on Friday with some of the bloc\'s ministers responsible for \r\nfisheries to discuss the state of play in the trade discussions with \r\nBritain, an EU official said.</p>', 'active', '2020-11-27 16:53:25', '2020-11-27 18:13:33');

-- --------------------------------------------------------

--
-- Table structure for table `news_techical_analyses`
--

CREATE TABLE `news_techical_analyses` (
  `id` int UNSIGNED NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_id` int UNSIGNED NOT NULL,
  `categories_id` int UNSIGNED NOT NULL,
  `body` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news_techical_analyses`
--

INSERT INTO `news_techical_analyses` (`id`, `image`, `title`, `description`, `lang_id`, `categories_id`, `body`, `status`, `created_at`, `updated_at`) VALUES
(1, '1607241798.jpg', 'dfgdfg', 'dfgdfg', 1, 1, '<p>dfgdfgdfg</p>', 'active', '2020-12-06 13:03:18', '2020-12-06 13:03:18');

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE `notes` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_id` int UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notes`
--

INSERT INTO `notes` (`id`, `name`, `title`, `description`, `lang_id`, `created_at`, `updated_at`) VALUES
(1, 'name', 'title', 'des', 1, NULL, NULL),
(2, 'اسم', 'تايتل', 'ديسكربشن ', 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `our_traders_news`
--

CREATE TABLE `our_traders_news` (
  `id` int UNSIGNED NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_id` int UNSIGNED NOT NULL,
  `categories_id` int UNSIGNED NOT NULL,
  `user_id` int UNSIGNED NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `our_traders_news__techical_analyses`
--

CREATE TABLE `our_traders_news__techical_analyses` (
  `id` int UNSIGNED NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_id` int UNSIGNED NOT NULL,
  `categories_id` int UNSIGNED NOT NULL,
  `user_id` int UNSIGNED NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `our_traders_news__techical_analyses`
--

INSERT INTO `our_traders_news__techical_analyses` (`id`, `image`, `title`, `lang_id`, `categories_id`, `user_id`, `description`, `body`, `status`, `created_at`, `updated_at`) VALUES
(1, '1606651941.jpg', 'said forex in  english', 1, 1, 7, 'said  forex', '<p>said&nbsp;&nbsp;&nbsp; test&nbsp; <br></p>', 'active', '2020-11-29 17:12:21', '2020-11-29 17:12:21'),
(2, '1606651975.jpeg', 'سيد  فوركس  فى الاعربى', 2, 2, 7, 'سيد  فى  العربى', '<p>سيدد&nbsp; فى&nbsp; العربى&nbsp; <br></p>', 'active', '2020-11-29 17:12:55', '2020-11-29 17:12:55'),
(3, '1606652051.jpg', 'mohamed  forex  english', 1, 1, 6, 'mohamed in  forexd', '<p>forex&nbsp; mohamed&nbsp; <br></p>', 'active', '2020-11-29 17:14:11', '2020-11-29 17:14:11'),
(4, '1606652081.jpg', 'محمد  فوركس  عربى', 2, 2, 6, 'محمد  فوركس', '<p>محمد&nbsp; فوريكس&nbsp; <br></p>', 'active', '2020-11-29 17:14:41', '2020-11-29 17:14:41'),
(5, '1606660894.jpg', 'dfsdf', 2, 2, 7, 'sdfsdf', '<p>sdfsdfsdf</p>', 'active', '2020-11-29 19:41:35', '2020-11-29 19:41:35');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `for` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `title`, `for`, `created_at`, `updated_at`) VALUES
(1, 'add admin', 'manager', NULL, NULL),
(2, 'edit admin', 'manager', NULL, NULL),
(3, 'delete admin', 'manager', NULL, NULL),
(4, 'show admins', 'manager', NULL, NULL),
(5, 'show admins', 'manager', NULL, NULL),
(6, 'add roles', 'roles', NULL, NULL),
(7, 'edit roles', 'roles', NULL, NULL),
(8, 'delete roles', 'roles', NULL, NULL),
(9, 'show roles', 'roles', NULL, NULL),
(10, 'show roles', 'roles', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int UNSIGNED NOT NULL,
  `role_id` int NOT NULL,
  `permission_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`id`, `role_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(1, 1, 0, '2020-11-25 16:08:34', '2020-11-25 16:08:34'),
(2, 1, 1, '2020-11-25 16:08:35', '2020-11-25 16:08:35'),
(3, 1, 2, '2020-11-25 16:08:35', '2020-11-25 16:08:35'),
(4, 1, 3, '2020-11-25 16:08:35', '2020-11-25 16:08:35'),
(5, 1, 4, '2020-11-25 16:08:35', '2020-11-25 16:08:35'),
(6, 1, 5, '2020-11-25 16:08:36', '2020-11-25 16:08:36'),
(7, 1, 6, '2020-11-25 16:08:36', '2020-11-25 16:08:36'),
(8, 1, 7, '2020-11-25 16:08:36', '2020-11-25 16:08:36'),
(9, 1, 8, '2020-11-25 16:08:36', '2020-11-25 16:08:36'),
(10, 1, 9, '2020-11-25 16:08:36', '2020-11-25 16:08:36');

-- --------------------------------------------------------

--
-- Table structure for table `providers`
--

CREATE TABLE `providers` (
  `id` int UNSIGNED NOT NULL,
  `provider_id` int NOT NULL,
  `provider` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_id` int UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reports`
--

INSERT INTO `reports` (`id`, `name`, `description`, `image`, `lang_id`, `created_at`, `updated_at`) VALUES
(1, 'yousry edit', 'yousry edit', '1606640824.jpg', 1, NULL, '2020-11-29 14:07:04'),
(2, 'يسرى', 'يسرى', '1606640850.jpg', 2, NULL, '2020-11-29 14:07:30');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'manager', '2020-11-25 16:08:34', '2020-11-25 16:08:34'),
(2, 'admins', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int UNSIGNED NOT NULL,
  `namesetting` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` tinyint DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_id` int UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `title`, `description`, `image`, `lang_id`, `created_at`, `updated_at`) VALUES
(3, 'سليدر 1', '2', '1606648951.jpeg', 1, '2020-11-29 16:22:31', '2020-12-10 13:58:33'),
(4, 'slider 2', '2', '1606648972.jpg', 1, '2020-11-29 16:22:52', '2020-12-10 14:00:50'),
(5, 'sdfsdf', '2', '1606649657.jpg', 2, '2020-11-29 16:34:17', '2020-12-10 13:55:17'),
(6, 'عرب', 'عربى', '1607592279.jpg', 2, '2020-12-10 14:24:40', '2020-12-10 14:24:40'),
(7, 'عربى3', 'عربى3', '1607592601.jpg', 2, '2020-12-10 14:30:01', '2020-12-10 14:30:01');

-- --------------------------------------------------------

--
-- Table structure for table `stories`
--

CREATE TABLE `stories` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_id` int UNSIGNED NOT NULL,
  `body` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stories`
--

INSERT INTO `stories` (`id`, `name`, `description`, `image`, `lang_id`, `body`, `created_at`, `updated_at`) VALUES
(1, 'yousry 1', 'yousry 1', '1606469268.jpg', 1, '<p>erterterterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr</p>', '', '2020-12-06 13:55:32'),
(2, 'yousry 2 fsdfsdf', 'yousry 2 sdfsdfsdf', '1606469293.jpg', 1, '', '', '2020-11-27 09:28:13'),
(3, 'yousry3', 'yousry3', '1606470053.jpg', 1, '', '', '2020-11-27 09:40:54'),
(4, 'عربى', 'عربى', '1606641581.jpg', 2, '<p>عربى</p>', '', '2020-12-10 09:27:36'),
(5, 'عربى', 'عربى', '1606641593.jpg', 2, '<p>عربى</p>', '', '2020-12-10 09:27:55'),
(6, 'yousry4', 'yousry4', '1606641618.jpg', 1, '', '', '2020-12-06 11:30:05');

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `jobname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `name`, `jobname`, `description`, `image`, `created_at`, `updated_at`) VALUES
(2, 'yousry', 'Developer', 'yousry', '1606398343.jpg', '2020-11-26 18:45:44', '2020-11-26 18:45:44'),
(3, 'tets', 'tetretert', 'efrwerwerwerwer', '1607254175.jpg', '2020-12-06 16:29:35', '2020-12-06 16:29:35');

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE `type` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `type`
--

INSERT INTO `type` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'vister ', NULL, NULL),
(2, 'editor  ', NULL, NULL),
(3, 'economy analysis', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int UNSIGNED NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_id` int UNSIGNED DEFAULT NULL,
  `verified` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `image`, `name`, `lastname`, `phone`, `email`, `password`, `type_id`, `verified`, `remember_token`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, NULL, 'user', NULL, NULL, 'user@user.com', '$2y$10$nFDzviDAU1kyO7l1Vja1BeicGG3TCRNwgu/91xnozoLxEqSLKDA/u', NULL, '1', NULL, NULL, NULL, NULL),
(3, '1606302709338image_profile.jpg', 'ahmed', NULL, NULL, 'ahmed@ahmed.com', '$2y$10$sSi0eN0ldEMYUf0yp.5AxeUeoT1fkanH2MMne81fL5sbOpMu7uovi', NULL, NULL, NULL, NULL, '2020-11-25 16:11:49', '2020-11-25 16:11:49'),
(6, '1606304670545image_profile.jpeg', 'moahmed', 'ali', '01258589887', 'mohamed@mohamed.com', '$2y$10$TnBEWGID5buHZvpt.ze6ieGGIXN0qvh7GyCPIsOj4wI3P1sDOIFRC', 3, '1', NULL, NULL, '2020-11-25 16:44:30', '2020-11-25 16:44:30'),
(7, '1606651790100image_profile.jpg', 'said', 'said', '01276794024', 'said@said.com', '$2y$10$lyymqSO5dskgR.Ih0dNHZueUQLCeq83FluIj4b.Mp3EoR7xD.3le6', 3, '1', NULL, NULL, '2020-11-29 17:09:50', '2020-11-29 17:09:50');

-- --------------------------------------------------------

--
-- Table structure for table `user_diractions`
--

CREATE TABLE `user_diractions` (
  `id` int UNSIGNED NOT NULL,
  `user_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_diractions` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `id` int UNSIGNED NOT NULL,
  `user_id` int UNSIGNED NOT NULL,
  `role_id` int UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`id`, `user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 3, 1, NULL, NULL),
(5, 6, 1, NULL, NULL),
(6, 7, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `u_roles`
--

CREATE TABLE `u_roles` (
  `id` int UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `u_roles`
--

INSERT INTO `u_roles` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'user_roles', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `verify_users`
--

CREATE TABLE `verify_users` (
  `user_id` int NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `verify_users`
--

INSERT INTO `verify_users` (`user_id`, `token`, `created_at`, `updated_at`) VALUES
(3, '0IhCygcneXMpSnkE8uJ7z36unTO0GHwIsajL5tvQ', '2020-11-25 16:11:49', '2020-11-25 16:11:49'),
(4, 'KRMqcVWcuLrOgpBWk847Pes3qKeCMZ0uKjVs5Byg', '2020-11-25 16:39:47', '2020-11-25 16:39:47'),
(5, 'o4FBU6wssshE47AsEtHUpM0nFVBNWgicUvc06uab', '2020-11-25 16:41:26', '2020-11-25 16:41:26'),
(6, 'qYo9RLU5zzAZHW4PCw7MvM8amheyWFXruwDOMkkS', '2020-11-25 16:44:31', '2020-11-25 16:44:31'),
(7, '3iGObxo8LgLfXVZzictsKJEimwqtBkIzJynDzwOr', '2020-11-29 17:09:51', '2020-11-29 17:09:51');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_role`
--
ALTER TABLE `admin_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_role_admin_id_foreign` (`admin_id`),
  ADD KEY `admin_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_lang_id_foreign` (`lang_id`);

--
-- Indexes for table `categories_techical_analyses`
--
ALTER TABLE `categories_techical_analyses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_techical_analyses_lang_id_foreign` (`lang_id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `galleries_lang_id_foreign` (`lang_id`),
  ADD KEY `galleries_categorie_id_foreign` (`categorie_id`);

--
-- Indexes for table `gallerycategories`
--
ALTER TABLE `gallerycategories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gallerycategories_lang_id_foreign` (`lang_id`);

--
-- Indexes for table `image_news`
--
ALTER TABLE `image_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `learncategories`
--
ALTER TABLE `learncategories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `learncategories_lang_id_foreign` (`lang_id`),
  ADD KEY `learncategories_menucategory_id_foreign` (`menuCategory_id`);

--
-- Indexes for table `learns`
--
ALTER TABLE `learns`
  ADD PRIMARY KEY (`id`),
  ADD KEY `learns_lang_id_foreign` (`lang_id`),
  ADD KEY `learns_categories_id_foreign` (`categories_id`);

--
-- Indexes for table `menu_categories`
--
ALTER TABLE `menu_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_categories_lang_id_foreign` (`lang_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `news_lang_id_foreign` (`lang_id`),
  ADD KEY `news_categories_id_foreign` (`categories_id`);

--
-- Indexes for table `news_techical_analyses`
--
ALTER TABLE `news_techical_analyses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `news_techical_analyses_lang_id_foreign` (`lang_id`),
  ADD KEY `news_techical_analyses_categories_id_foreign` (`categories_id`);

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notes_lang_id_foreign` (`lang_id`);

--
-- Indexes for table `our_traders_news`
--
ALTER TABLE `our_traders_news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `our_traders_news_lang_id_foreign` (`lang_id`),
  ADD KEY `our_traders_news_categories_id_foreign` (`categories_id`),
  ADD KEY `our_traders_news_user_id_foreign` (`user_id`);

--
-- Indexes for table `our_traders_news__techical_analyses`
--
ALTER TABLE `our_traders_news__techical_analyses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `our_traders_news__techical_analyses_lang_id_foreign` (`lang_id`),
  ADD KEY `our_traders_news__techical_analyses_categories_id_foreign` (`categories_id`),
  ADD KEY `our_traders_news__techical_analyses_user_id_foreign` (`user_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `providers`
--
ALTER TABLE `providers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_lang_id_foreign` (`lang_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sliders_lang_id_foreign` (`lang_id`);

--
-- Indexes for table `stories`
--
ALTER TABLE `stories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stories_lang_id_foreign` (`lang_id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_type_id_foreign` (`type_id`);

--
-- Indexes for table `user_diractions`
--
ALTER TABLE `user_diractions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_roles_user_id_foreign` (`user_id`),
  ADD KEY `user_roles_role_id_foreign` (`role_id`);

--
-- Indexes for table `u_roles`
--
ALTER TABLE `u_roles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `admin_role`
--
ALTER TABLE `admin_role`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `categories_techical_analyses`
--
ALTER TABLE `categories_techical_analyses`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `gallerycategories`
--
ALTER TABLE `gallerycategories`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `image_news`
--
ALTER TABLE `image_news`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `learncategories`
--
ALTER TABLE `learncategories`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `learns`
--
ALTER TABLE `learns`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `menu_categories`
--
ALTER TABLE `menu_categories`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `news_techical_analyses`
--
ALTER TABLE `news_techical_analyses`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `notes`
--
ALTER TABLE `notes`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `our_traders_news`
--
ALTER TABLE `our_traders_news`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `our_traders_news__techical_analyses`
--
ALTER TABLE `our_traders_news__techical_analyses`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `providers`
--
ALTER TABLE `providers`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `stories`
--
ALTER TABLE `stories`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `type`
--
ALTER TABLE `type`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user_diractions`
--
ALTER TABLE `user_diractions`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `u_roles`
--
ALTER TABLE `u_roles`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin_role`
--
ALTER TABLE `admin_role`
  ADD CONSTRAINT `admin_role_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `admin_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_lang_id_foreign` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `categories_techical_analyses`
--
ALTER TABLE `categories_techical_analyses`
  ADD CONSTRAINT `categories_techical_analyses_lang_id_foreign` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `galleries`
--
ALTER TABLE `galleries`
  ADD CONSTRAINT `galleries_categorie_id_foreign` FOREIGN KEY (`categorie_id`) REFERENCES `gallerycategories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `galleries_lang_id_foreign` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `gallerycategories`
--
ALTER TABLE `gallerycategories`
  ADD CONSTRAINT `gallerycategories_lang_id_foreign` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `learncategories`
--
ALTER TABLE `learncategories`
  ADD CONSTRAINT `learncategories_lang_id_foreign` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `learncategories_menucategory_id_foreign` FOREIGN KEY (`menuCategory_id`) REFERENCES `menu_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `learns`
--
ALTER TABLE `learns`
  ADD CONSTRAINT `learns_categories_id_foreign` FOREIGN KEY (`categories_id`) REFERENCES `learncategories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `learns_lang_id_foreign` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_categories`
--
ALTER TABLE `menu_categories`
  ADD CONSTRAINT `menu_categories_lang_id_foreign` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `news_categories_id_foreign` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `news_lang_id_foreign` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `news_techical_analyses`
--
ALTER TABLE `news_techical_analyses`
  ADD CONSTRAINT `news_techical_analyses_categories_id_foreign` FOREIGN KEY (`categories_id`) REFERENCES `categories_techical_analyses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `news_techical_analyses_lang_id_foreign` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `notes`
--
ALTER TABLE `notes`
  ADD CONSTRAINT `notes_lang_id_foreign` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `our_traders_news`
--
ALTER TABLE `our_traders_news`
  ADD CONSTRAINT `our_traders_news_categories_id_foreign` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `our_traders_news_lang_id_foreign` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `our_traders_news_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `our_traders_news__techical_analyses`
--
ALTER TABLE `our_traders_news__techical_analyses`
  ADD CONSTRAINT `our_traders_news__techical_analyses_categories_id_foreign` FOREIGN KEY (`categories_id`) REFERENCES `categories_techical_analyses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `our_traders_news__techical_analyses_lang_id_foreign` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `our_traders_news__techical_analyses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `reports`
--
ALTER TABLE `reports`
  ADD CONSTRAINT `reports_lang_id_foreign` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sliders`
--
ALTER TABLE `sliders`
  ADD CONSTRAINT `sliders_lang_id_foreign` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `stories`
--
ALTER TABLE `stories`
  ADD CONSTRAINT `stories_lang_id_foreign` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `u_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
