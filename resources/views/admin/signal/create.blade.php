@extends('admin.layouts.master')
@section('title')
    add Signal
@endsection

@section('page-header')
    <section class="content-header">
        <h1>
            add Signal
            <small></small>
        </h1>

    </section>
@endsection
<script src="{{ asset('editor/build/jodit.js')}}"></script>
<script src="{{ asset('editor/js/sample.js')}}"></script>
<script src="{{ asset('editor/build/jodit.js')}}"></script>

	<link rel="stylesheet" type="text/css" href="{{ asset('editor/app.css')}}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('editor/build/jodit.min.css')}}" />

@section('content')

    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">add Signal </h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->









                    <form class="form-horizontal" method="post" enctype="multipart/form-data"  id="dynamic_form" >
                        {{csrf_field()}}
                    <div class="box-body">







                       <span id="result"></span>
                       <table class="table table-bordered table-striped" id="user_table">
                      <thead>
                      <tr>
                          <th width="35%">take profit</th>

                          <th width="30%">Action</th>
                      </tr>
                      </thead>
                      <tbody>

                      </tbody>
                      <tfoot>
                      <tr>
                                      <td colspan="2" align="right">&nbsp;</td>
                                      <div class="form-group">

                                          <label for="name" class="col-sm-4 control-label">currancryname</label>

                                          <div class="col-sm-4">
                                              <input type="text" name="currancryname" class="form-control" id="name" placeholder="currancryname" value="{{ old('currancryname') }}">
                                          </div>

                                      </div>


                                      <div class="form-group">

                                          <label for="name" class="col-sm-4 control-label">enter point</label>

                                          <div class="col-sm-4">
                                              <input type="text" name="enterpoint" class="form-control" id="name" placeholder="jobname" value="{{ old('enterpoint') }}">
                                          </div>

                                      </div>



                                      <div class="form-group">

                                          <label for="name" class="col-sm-4 control-label">stop losing</label>

                                          <div class="col-sm-4">
                                              <input type="text" name="stoplosing" class="form-control" id="stoplosing" placeholder="stoplosing" value="{{ old('stoplosing') }}">
                                          </div>

                                      </div>


                                      <div class="form-group">
                             <label for="category" class="col-sm-4 control-label">Gallery categorys</label>
                             <div class="col-sm-4">
                             <select name="category" id="category" class="select2 form-control " >
                               @foreach($categorys as $category)
                                       <option value="{{$category->id}}">{{$category->name}}</option>
                               @endforeach
                             </select>
                             </div>
                             </div>



                                      <input type="file" name="image" class="form-control" />






                                      <td>

                        @csrf
                        <input type="submit" name="save" id="save" class="btn btn-primary" value="Save" />
                       </td>
                      </tr>
                      </tfoot>
                      </table>
                      </form>





                    </div>





                </div>

            </div>
        </div>
    </section>

@endsection

@section('css')

    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css')}}">
@endsection

@section('js')

    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.min.js')}}"></script>


    <script>
        $('.select2').select2()
    </script>

    <script>
      const editor = Jodit.make('#editor' ,{
        uploader: {
          url: 'https://xdsoft.net/jodit/connector/index.php?action=fileUpload'
        },
        filebrowser: {
          ajax: {
            url: 'https://xdsoft.net/jodit/connector/index.php'
          }
        }
      });
    </script>


    <script>
    $(document).ready(function(){

     var count = 1;

     dynamic_field(count);

     function dynamic_field(number)
     {
      html = '<tr>';
            html += '<td><input type="text" name="takeprofit_id[]" class="form-control" /></td>';


            if(number > 1)
            {
                html += '<td><button type="button" name="remove" id="" class="btn btn-danger remove">Remove</button></td></tr>';
                $('tbody').append(html);
            }
            else
            {
                html += '<td><button type="button" name="add" id="add" class="btn btn-success">Add</button></td></tr>';
                $('tbody').html(html);
            }
     }

     $(document).on('click', '#add', function(){
      count++;
      dynamic_field(count);
     });

     $(document).on('click', '.remove', function(){
      count--;
      $(this).closest("tr").remove();
     });














     $('#dynamic_form').submit(function(e) {
                e.preventDefault();
                let formData = new FormData(this);
                $('#image-input-error').text('');

                $.ajax({
                   type:'POST',
                   url: `{{ url("/admin/signal") }}`,
                    data: formData,
                    contentType: false,
                    processData: false,
                    success:function(data)
                    {
                        if(data.error)
                        {
                            var error_html = '';
                            for(var count = 0; count < data.error.length; count++)
                            {
                                error_html += '<p>'+data.error[count]+'</p>';
                            }
                            $('#result').html('<div class="alert alert-danger">'+error_html+'</div>');
                        }
                        else
                        {
                            dynamic_field(1);
                            $('#result').html('<div class="alert alert-success">'+data.success+'</div>');
                        }
                        // $('#save').attr('disabled', false);
                    }

                });

           });











    });
    </script>
@endsection
