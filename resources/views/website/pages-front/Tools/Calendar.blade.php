<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
@include('website.includes.css')
<!-- Title -->
<title>smart-deal24 - {{$title}}</title>
<!-- Favicon -->
<link href="images/favicon.ico" sizes="128x128" rel="shortcut icon" type="image/x-icon" />
<link href="images/favicon.ico" sizes="128x128" rel="shortcut icon" />

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="wrapper">
	<div id="preloader" class="preloader">
		<div id="pre" class="preloader_container"><div class="preloader_disabler btn btn-default">Disable Preloader</div></div>
	</div>
  @include('website.includes.header')

	<!-- Home Design Inner Pages -->
	<div class="ulockd-inner-home">
		<div class="container text-center">
			<div class="row">
				<div class="inner-conraimer-details">
					<div class="col-md-12">
						<h1 class="text-uppercase">Team</h1>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Home Design Inner Pages -->
	<div class="ulockd-inner-page">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="ulockd-icd-layer">
						<ul class="list-inline ulockd-icd-sub-menu">
							<li><a href="#"> HOME </a></li>
							<li><a href="#"> > </a></li>
							<li> <a href="#"> TEAM </a> </li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<section class="our-team">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center">
					<div class="ulockd-main-title">
						<h2>Our Calendar</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem labore voluptates consequuntur velit maiores fugiat eaque.</p>
					</div>
				</div>
			</div>


			<!-- TradingView Widget BEGIN -->
<div class="tradingview-widget-container">
  <div class="tradingview-widget-container__widget"></div>
  <div class="tradingview-widget-copyright"><a href="https://www.tradingview.com/markets/currencies/economic-calendar/" rel="noopener" target="_blank"><span class="blue-text">Economic Calendar</span></a> by TradingView</div>
  <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-events.js" async>
  {
  "colorTheme": "light",
  "isTransparent": false,
  "width": "1000",
  "height": "1000",
  "locale": "en",
  "importanceFilter": "-1,0,1"
}
  </script>
</div>
<!-- TradingView Widget END -->

		</div>
	</section>

	<!-- Our Partner -->
	<section class="parallax ulockd_bgi4 bgc-overlay-white75 ulockd-pad650" data-stellar-background-ratio="0.3">
		<div class="container">
			<div class="row">
				<div class="col-xs-6 col-sm-4 col-md-2"><div class="ulockd-divider-thumb text-center"><img class="img-responsive" src="images/partners/1.png" alt="1.png"></div></div>
				<div class="col-xs-6 col-sm-4 col-md-2"><div class="ulockd-divider-thumb text-center"><img class="img-responsive" src="images/partners/2.png" alt="2.png"></div></div>
				<div class="col-xs-6 col-sm-4 col-md-2"><div class="ulockd-divider-thumb text-center"><img class="img-responsive" src="images/partners/3.png" alt="3.png"></div></div>
				<div class="col-xs-6 col-sm-4 col-md-2"><div class="ulockd-divider-thumb text-center"><img class="img-responsive" src="images/partners/4.png" alt="4.png"></div></div>
				<div class="col-xs-6 col-sm-4 col-md-2"><div class="ulockd-divider-thumb text-center"><img class="img-responsive" src="images/partners/5.png" alt="5.png"></div></div>
				<div class="col-xs-6 col-sm-4 col-md-2"><div class="ulockd-divider-thumb text-center"><img class="img-responsive" src="images/partners/6.png" alt="6.png"></div></div>
			</div>
		</div>
	</section>

@include('website.includes.footer')

<!-- <a class="scrollToHome ulockd-bgthm" href="#"><i class="fa fa-home"></i></a> -->
</div>
@include('website.includes.js')
</body>

</html>
