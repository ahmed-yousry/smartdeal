<!DOCTYPE html>
<html dir="ltr" lang="en">

 
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- css file -->
@include('website.includes.css')

<!-- Title -->
<title>Smart-Deal24 - {{$title}}</title>
<!-- Favicon -->
<link href="images/favicon.ico" sizes="128x128" rel="shortcut icon" type="image/x-icon" />
<link href="images/favicon.ico" sizes="128x128" rel="shortcut icon" />

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="wrapper">
	<div id="preloader" class="preloader">
		<div id="pre" class="preloader_container"><div class="preloader_disabler btn btn-default">Disable Preloader</div></div>
	</div>


  @include('website.includes.header')

	<!-- Home Design Inner Pages -->
	<div class="ulockd-inner-home">
		<div class="container text-center">
			<div class="row">
				<div class="inner-conraimer-details">
					<div class="col-md-12">
						<h1 class="text-uppercase">LATEST






{{$get_news->first()->get_categories()->first()->name}}


						</h1>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Home Design Inner Pages -->
	<div class="ulockd-inner-page">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="ulockd-icd-layer">
						<ul class="list-inline ulockd-icd-sub-menu">
							<li><a href="#"> HOME </a></li>
							<li><a href="#"> > </a></li>
							<li> <a href="#">{{$get_news->first()->get_categories()->first()->name}} News </a> </li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Home Design Inner Pages -->
	<section class="ulockd-ip-latest-news">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center">
					<div class="ulockd-main-title">
						<h2>Our {{@$get_news->first()->get_categories()->first()->name}} News</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem labore voluptates consequuntur velit maiores fugiat eaque.</p>
					</div>
				</div>
			</div>
			<div class="row">

            @foreach ($get_news as $get_new)
				<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4">
					<article class="blog-post">
						<div class="post-thumb">
							<img class="img-responsive img-whp" src="{{url('/uplodes/newsphoto/'.@$get_new->image)}}" alt="1.jpg">


						</div>
						<div class="details">
							<div class="post-date text-thm2">{{$get_new->created_at}}</div>
							<h3 class="post-title">{{$get_new->title}}</h3>
							<p>{{ Str::limit($get_new->description, 150) }} </p>
							<a  class="btn btn-lg ulockd-btn-white" href="{{url(LaravelLocalization::setLocale().'/TechicalAnalysis_Details/'.$get_new->id)}}">Read More </a>
						</div>
					</article>
				</div>
@endforeach
			</div>
		</div>
	</section>

	<!-- Our Footer -->
@include('website.includes.footer')

<!-- <a class="scrollToHome ulockd-bgthm" href="#"><i class="fa fa-home"></i></a> -->
</div>
<!-- Wrapper End -->
@include('website.includes.js')
</body>

 
</html>
