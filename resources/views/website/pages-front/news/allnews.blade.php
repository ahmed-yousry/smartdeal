<!DOCTYPE html>
<html dir="ltr" lang="en">

 
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- css file -->
@include('website.includes.css')

<!-- Title -->
<title>Smart-Deal24 - {{$title}}</title>
<!-- Favicon -->
<link href="images/favicon.ico" sizes="128x128" rel="shortcut icon" type="image/x-icon" />
<link href="images/favicon.ico" sizes="128x128" rel="shortcut icon" />

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="wrapper">
	<div id="preloader" class="preloader">
		<div id="pre" class="preloader_container"><div class="preloader_disabler btn btn-default">Disable Preloader</div></div>
	</div>


  @include('website.includes.header')

	<!-- Home Design Inner Pages -->
	<div class="ulockd-inner-home">
		<div class="container text-center">
			<div class="row">
				<div class="inner-conraimer-details">
					<div class="col-md-12">
						<h1 class="text-uppercase">LATEST






{{$get_news->first()->get_categories()->first()->name}}


						</h1>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Home Design Inner Pages -->
	<div class="ulockd-inner-page">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="ulockd-icd-layer">
						<ul class="list-inline ulockd-icd-sub-menu">
							<li><a href="#"> HOME </a></li>
							<li><a href="#"> > </a></li>
							<li> <a href="#">{{$get_news->first()->get_categories()->first()->name}} News </a> </li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="error" hidden>
	</div>



	<!-- Home Design Inner Pages -->
	<section class="ulockd-ip-latest-news">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center">
					<div class="ulockd-main-title">
						<h2>Our {{@$get_news->first()->get_categories()->first()->name}} News</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem labore voluptates consequuntur velit maiores fugiat eaque.</p>
					</div>
				</div>
			</div>




<div class="row">
  <div class="col-md-12">
      <!-- Masonry Filter -->
      <ul class="list-inline masonry-filter text-center">
          <li><a href="#" class="active" data-filter="*">All</a></li>

@foreach($get_category_news as $get_category_new)
          <li><a href="#" class="a" data-filter=".{{$get_category_new->showname}}" class="">{{$get_category_new->name}}</a></li>
@endforeach
      </ul>
      <!-- End Masonry Filter -->

      <!-- Masonry Grid -->
      <div id="grid" class="masonry-gallery grid-4 clearfix">

        <!-- Masonry Item -->
        <!-- <div class="isotope-item experts consulting">
            <div class="gallery-thumb">
              <img class="img-responsive img-whp" src="images/gallery/9.jpg" alt="project">
              <div class="overlayer">
      <div class="lbox-caption">
        <div class="lbox-details">
          <h5>Gallery Title Here</h5>
          <ul class="list-inline">
            <li>
              <a class="popup-img" href="{{ asset('assets/website/images/gallery/index4.jpeg')}}" title="Gallery Photos"><span class="flaticon-add-square-button"></span></a>
            </li>
            <li>
              <a class="link-btn" href="#" ><span class="flaticon-link-symbol"></span></a>
            </li>
          </ul>
        </div>
      </div>
              </div>
            </div>
        </div> -->






@foreach($get_news as $get_new)

        <div class="isotope-item {{$get_new->get_categories()->first()->showname}}" >


            <article class="blog-post" >
              <div class="post-thumb">




      <a href="{{url(LaravelLocalization::setLocale().'/Forex_Details/'.$get_new->id)}}">
        <image src="{{url('/uplodes/newsphoto/'.@$get_new->image)}}" style="padding: 10px;"    alt="{{url('/uplodes/newsphoto/'.@$get_new->image)}}" /> </a>




                </div>
                <div class="details">
                  <div class="post-date text-thm2">{{$get_new->created_at}}</div>
                  <a href="{{url(LaravelLocalization::setLocale().'/Forex_Details/'.$get_new->id)}}"><h3 class="post-title">{{ Str::limit($get_new->title, 50) }} </h3></a>
                  <p>{{ Str::limit($get_new->description, 60) }}  </p>
                  <a  class="btn btn-lg ulockd-btn-white" href="{{url(LaravelLocalization::setLocale().'/Forex_Details/'.$get_new->id)}}">Read More </a>
                </div>
              </article>



          </div>


@endforeach


        <!-- Masonry = Masonry Item -->
      </div>
      <!-- Masonry Gallery Grid Item -->
  </div>
</div>
			<div class="row">


	</section>

	<!-- Our Footer -->
@include('website.includes.footer')


</div>
<!-- Wrapper End -->

@include('website.includes.js')

</body>

 
</html>
