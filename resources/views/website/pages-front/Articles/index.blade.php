<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- css file -->
@include('website.includes.css')
<!-- Title -->
<title>smart-deal24 - {{$title}}</title>
<!-- Favicon -->
<link href="images/favicon.ico" sizes="128x128" rel="shortcut icon" type="image/x-icon" />
<link href="images/favicon.ico" sizes="128x128" rel="shortcut icon" />

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="wrapper">
	<div id="preloader" class="preloader">
		<div id="pre" class="preloader_container"><div class="preloader_disabler btn btn-default">Disable Preloader</div></div>
	</div>

@include('website.includes.header')
	<!-- Home Design Inner Pages -->
	<div class="ulockd-inner-home">
		<div class="container text-center">
			<div class="row">
				<div class="inner-conraimer-details">
					<div class="col-md-12">
						<h1 class="text-uppercase">All Article</h1>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Home Design Inner Pages -->
	<div class="ulockd-inner-page">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="ulockd-icd-layer">
						<ul class="list-inline ulockd-icd-sub-menu">
							<li><a href="#"> HOME </a></li>
							<li><a href="#"> > </a></li>
							<li> <a href="#"> PROJECT </a> </li>
							<li><a href="#"> > </a></li>
							<li> <a href="#"> GRID </a> </li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>





	<!-- Our Feature Project -->
	<section id="project" class="our-project ulockd_bgp1">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center">
					<div class="ulockd-main-title">
						<h2>All  Article</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem labore voluptates consequuntur velit maiores fugiat eaque.</p>
					</div>
				</div>
			</div>
			<div class="row">


	@foreach ($storys as $story)
				<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4">
					<div class="project-box text-center">
						<div class="pb-thumb">
							<img class="img-responsive img-whp" src="{{url('/uplodes/newsphoto/'.$story->image)}}" alt="1.jpg">
						</div>
						<div class="pb-details">
							<h3>{{$story->title}}</h3>
							<p>{{$story->description}}</p>
							<a class="btn ulockd-btn-thm2" href="{{url(LaravelLocalization::setLocale().'/Articles/'.$story->id)}}">Read More</a>
						</div>
					</div>
				</div>


@endforeach





			</div>
			<div class="row ulockd-mrgn1240">
				<div class="col-md-12 text-center">
					<nav aria-label="Page navigation navigation-lg">
					    <ul class="pagination">
					    	<li>
					        	<a href="#" aria-label="Previous">
					        		<span aria-hidden="true">PREV</span>
					        	</a>
					    	</li>
					    	<li class="active"><a href="#">1</a></li>
					    	<li><a href="#">2</a></li>
					    	<li><a href="#">3</a></li>
					    	<li><a href="#">4</a></li>
					    	<li><a href="#">5</a></li>
					    	<li>
					        	<a href="#" aria-label="Next">
					        		<span aria-hidden="true">NEXT</span>
					        	</a>
					    	</li>
					    </ul>
					</nav>
				</div>
			</div>
		</div>
	</section>

@include('website.includes.footer')

<!-- <a class="scrollToHome ulockd-bgthm" href="#"><i class="fa fa-home"></i></a> -->
</div>
<!-- Wrapper End -->
@include('website.includes.js')
</body>

</html>
