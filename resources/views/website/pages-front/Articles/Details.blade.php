<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- css file -->
@include('website.includes.css')
<!-- Title -->
<title>smart-deal24 - {{$title}}</title>
<!-- Favicon -->
<link href="images/favicon.ico" sizes="128x128" rel="shortcut icon" type="image/x-icon" />
<link href="images/favicon.ico" sizes="128x128" rel="shortcut icon" />

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="wrapper">
	<div id="preloader" class="preloader">
		<div id="pre" class="preloader_container"><div class="preloader_disabler btn btn-default">Disable Preloader</div></div>
	</div>

@include('website.includes.header')


	<!-- Home Design Inner Pages -->
	<div class="ulockd-inner-home">
		<div class="container text-center">
			<div class="row">
				<div class="inner-conraimer-details">
					<div class="col-md-12">
						<h1 class="text-uppercase">LATEST NEWS</h1>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Home Design Inner Pages -->
	<div class="ulockd-inner-page">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="ulockd-icd-layer">
						<ul class="list-inline ulockd-icd-sub-menu">
							<li><a href="#"> HOME </a></li>
							<li><a href="#"> > </a></li>
							<li> <a href="#"> LATEST NEWS </a> </li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Inner Pages Main Section -->
	<section class="ulockd-service-details">
		<div class="container">
			<div class="col-md-4 col-lg-3 ulockd-pdng0">
				<div class="widget-sidebar">


					<!-- TradingView Widget BEGIN -->
			<div class="tradingview-widget-container">
				<div class="tradingview-widget-container__widget"></div>
				<div class="tradingview-widget-copyright"><a href="https://www.tradingview.com/markets/currencies/" rel="noopener" target="_blank"><span class="blue-text">Forex</span></a> by TradingView</div>
				<script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-market-overview.js" async>
				{
				"colorTheme": "light",
				"dateRange": "12M",
				"showChart": true,
				"locale": "en",
				"largeChartUrl": "",
				"isTransparent": false,
				"showSymbolLogo": true,
				"width": "270",
				"height": "600",
				"plotLineColorGrowing": "rgba(33, 150, 243, 1)",
				"plotLineColorFalling": "rgba(33, 150, 243, 1)",
				"gridLineColor": "rgba(240, 243, 250, 1)",
				"scaleFontColor": "rgba(120, 123, 134, 1)",
				"belowLineFillColorGrowing": "rgba(33, 150, 243, 0.12)",
				"belowLineFillColorFalling": "rgba(33, 150, 243, 0.12)",
				"symbolActiveColor": "rgba(33, 150, 243, 0.12)",
				"tabs": [
					{
						"title": "Forex",
						"symbols": [
							{
								"s": "FX:EURUSD"
							},
							{
								"s": "FX:GBPUSD"
							},
							{
								"s": "FX:USDJPY"
							},
							{
								"s": "FX:USDCHF"
							},
							{
								"s": "FX:AUDUSD"
							},
							{
								"s": "FX:USDCAD"
							}
						],
						"originalTitle": "Forex"
					}
				]
			}
				</script>
			</div>
			<!-- TradingView Widget END -->








				</div>
			</div>
			<div class="col-md-8 col-lg-9">
				<div class="row">
					<div class="col-md-12 ulockd-mrgn1210">
						<div class="ulockd-project-sm-thumb">
							<img class="img-responsive img-whp" src="{{url('/uplodes/newsphoto/'.$storys->first()->image)}}" alt="">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12 ulockd-mrgn1210">
						<div class="ulockd-pd-content">
							<div class="ulockd-bp-date">
								<ul class="list-inline">
									<li class="ulockd-bp-date-innner">On <a href="#"><span class="text-thm2">25</span> / Jun 2018</a></li>
									<li class="ulockd-bp-comment"><a href="#"><span class="flaticon-nurse-head text-thm1"></span> Ana D Cruse</a></li>
									<li class="ulockd-bp-comment"><a href="#"><span class="flaticon-chat text-thm1"></span> 05 Comment</a></li>
									<li class="ulockd-bp-comment"><a href="#"><span class="flaticon-black-check-box text-thm1"></span> Service</a></li>
								</ul>
							</div>
							<h3>{{$storys->first()->title}}<h3>


            @php
 echo $storys->first()->body;
@endphp
						</div>
					</div>

					<!-- <div class="col-md-12 ulockd-mrgn1210">
						<div class="ulockd-bpd-thumb">
							<img class="img-responsive img-whp" src="images/blog/blog-details2.jpg" alt="blog-details2.jpg">
						</div>
						<div class="row">
							<div class="col-md-6">
								<img class="img-responsive img-whp" src="images/blog/blog-details4.jpg" alt="blog-details4.jpg">
							</div>
							<div class="col-md-6">
								<img class="img-responsive img-whp" src="images/blog/blog-details5.jpg" alt="blog-details5.jpg">
							</div>
						</div><br>
						<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
						<blockquote class="ulockd-mrgn1220">
						    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla accusantium omnis, debitis veniam, at fugiat numquam iusto eos alias asperiores voluptate, explicabo doloremque eveniet mollitia non quisquam dolores fugit magni magnam, veritatis pariatur! Ut, temporibus.</p>
						    <footer>Vice President <cite title="Source Title"> Muhibbur Rashid</cite></footer>
						</blockquote>
					</div> -->
				</div>













			</div>
		</div>
	</section>


@include('website.includes.footer')

<!-- <a class="scrollToHome ulockd-bgthm" href="#"><i class="fa fa-home"></i></a> -->
</div>
<!-- Wrapper End -->
@include('website.includes.js')
</body>

</html>
