
<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- css file -->


<!-- <link rel="stylesheet" href="css/bootstrap-rtl.min.css"> -->
@include('website.includes.css')
<!-- Title -->
<title>Smart-deal24</title>
<!-- Favicon -->
<link href="images/favicon.ico" sizes="128x128" rel="shortcut icon" type="image/x-icon" />
<link href="images/favicon.ico" sizes="128x128" rel="shortcut icon" />

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

		<style type="text/css">
		.open-top-left{
			position: fixed;
			margin-left: 30px;
			margin-top: 30px;
			top: 0;
			left: 0;
		}

		.open-top-right{
			position: fixed;
			margin-right: 30px;
			margin-top: 30px;
			top: 0;
			right: 0;
		}

		.open-bottom-left{
			position: fixed;
			margin-left: 30px;
			margin-bottom: 30px;
			left: 0;
			bottom: 0;
		}

		.open-bottom-right{
			position: fixed;
			margin-right: 30px;
			margin-bottom: 30px;
			right: 0;
			bottom: 0;
		}

		.form-top-left{
			position: fixed;
			width: 18rem;
			margin-left: 30px;
			margin-top: 30px;
			top: 0;
			left: 0;
		}

		.form-top-right{
			position: fixed;
			width: 18rem;
			margin-right: 30px;
			margin-top: 30px;
			top: 0;
			right: 0;
		}

		.form-bottom-left{
			position: fixed;
			width: 18rem;
			margin-left: 30px;
			margin-bottom: 30px;
			bottom: 0;
			left: 0;
		}

		.form-bottom-right{
			position: fixed;
			width: 18rem;
			margin-right: 30px;
			margin-bottom: 30px;
			bottom: 0;
			right: 0;
		}
		</style>
</head>
<body>




@include('website.includes.header2')

















	@if(\Mcamara\LaravelLocalization\Facades\LaravelLocalization::getCurrentLocale()=='ar')
	<!-- Home Design -->



	<!-- Home Design -->
	<div class="ulockd-home-slider triangles">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 ulockd-pmz">
					<div class="main-slider2">
						@foreach ($get_sliders as $slider)
					<div class="item">
						<div class="mslider-caption text-left">
							<div class="mslider-details">
								<div class="slider-text1 color-white">{{$slider->title}}</div>
								<div class="slider-text2">{{$slider->description}}</span></div>
								<!-- <button class="btn btn-lg ulockd-btn-thm2 ulockd-mrgn1225">Learn More</button>
								<button class="btn btn-lg ulockd-btn-white ulockd-mrgn1225">Get a Quote</button> -->
							</div>
						</div>

						<img class="img-responsive img-whp" src="{{url('/uplodes/newsphoto/'.$slider->image)}}" alt="h3.jpg">
					</div>
					@endforeach

					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Our About -->
	<section class="ulockd-about ulockd-pad12650">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="about-box">
						<h2 class="ulockd-mrgn120 title-bottom"> {{$get_note->first()->name}}</h2>

						<p class="lead">{{$get_note->first()->title}}</p>
						<p>{{$get_note->first()->description}}</p>
						<img class="img-responsive h100" src="{{url('website/images/resource/signeture.png')}}" alt="signeture.png">
					</div>
				</div>
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12">
							<div class="three-grid-slider">

								@foreach ($get_storys as $get_story)
								<div class="item">
									<div class="about-box2 text-center">
										<div class="ab-thumb">
											<img class="img-responsive img-whp" style="height:320px;width:430px" src="{{url('/uplodes/newsphoto/'.@$get_story->image)}}" alt="s1.jpg">
											<div class="about-icon text-center"><i class="flaticon-classroom text-thm2"></i></div>
										</div>


										<div class="ab-details">
											<h4 class="fwb">{{$get_story->name}}</h4>
											<a class="btn btn-md ulockd-btn-thm2" href="{{url(LaravelLocalization::setLocale().'/Articles/'.$get_story->id)}}"> اقراء المزيد</a>
										</div>





									</div>
								</div>
@endforeach








							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="tradingview-widget-container">
			<div class="tradingview-widget-container__widget"></div>
			<script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-ticker-tape.js" async>
			{
			"symbols": [
				{
					"proName": "FOREXCOM:SPXUSD",
					"title": "S&P 500"
				},
				{
					"proName": "FOREXCOM:NSXUSD",
					"title": "Nasdaq 100"
				},
				{
					"proName": "FX_IDC:EURUSD",
					"title": "EUR/USD"
				},
				{
					"proName": "BITSTAMP:BTCUSD",
					"title": "BTC/USD"
				},
				{
					"proName": "BITSTAMP:ETHUSD",
					"title": "ETH/USD"
				}
			],
			"showSymbolLogo": true,
			"colorTheme": "light",
			"isTransparent": false,
			"displayMode": "adaptive",
			"locale": "en"
		}
			</script>
		</div>
	</section>

@else







	<!-- Home Design -->
	<div class="ulockd-home-slider triangles">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 ulockd-pmz">
					<div class="main-slider2">
						@foreach ($get_sliders as $slider)
					<div class="item">
						<div class="mslider-caption text-left">
							<div class="mslider-details">
								<div class="slider-text1 color-white">{{$slider->title}}</div>
								<div class="slider-text2">{{$slider->description}}</span></div>
								<!-- <button class="btn btn-lg ulockd-btn-thm2 ulockd-mrgn1225">Learn More</button>
								<button class="btn btn-lg ulockd-btn-white ulockd-mrgn1225">Get a Quote</button> -->
							</div>
						</div>

						<img class="img-responsive img-whp" src="{{url('/uplodes/newsphoto/'.$slider->image)}}" alt="h3.jpg">
					</div>
					@endforeach

					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Our About -->
	<section class="ulockd-about ulockd-pad12650">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="about-box">
						<h2 class="ulockd-mrgn120 title-bottom"> {{$get_note->first()->name}}</h2>

						<p class="lead">{{$get_note->first()->title}}</p>
						<p>{{$get_note->first()->description}}</p>
						<img class="img-responsive h100" src="{{url('website/images/resource/signeture.png')}}" alt="signeture.png">
					</div>
				</div>
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12">
							<div class="three-grid-slider">

								@foreach ($get_storys as $get_story)
								<div class="item">
									<div class="about-box2 text-center">
										<div class="ab-thumb">
											<img class="img-responsive img-whp" style="height:320px;width:430px" src="{{url('/uplodes/newsphoto/'.@$get_story->image)}}" alt="s1.jpg">
											<div class="about-icon text-center"><i class="flaticon-classroom text-thm2"></i></div>
										</div>


										<div class="ab-details">
											<h4 class="fwb">{{$get_story->name}}</h4>
											<a class="btn btn-md ulockd-btn-thm2" href="{{url(LaravelLocalization::setLocale().'/Articles/'.$get_story->id)}}"> اقراء المزيد</a>
										</div>





									</div>
								</div>
@endforeach








							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- <div class="tradingview-widget-container">
			<div class="tradingview-widget-container__widget"></div>
			<script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-ticker-tape.js" async>
			{
			"symbols": [
				{
					"proName": "FOREXCOM:SPXUSD",
					"title": "S&P 500"
				},
				{
					"proName": "FOREXCOM:NSXUSD",
					"title": "Nasdaq 100"
				},
				{
					"proName": "FX_IDC:EURUSD",
					"title": "EUR/USD"
				},
				{
					"proName": "BITSTAMP:BTCUSD",
					"title": "BTC/USD"
				},
				{
					"proName": "BITSTAMP:ETHUSD",
					"title": "ETH/USD"
				}
			],
			"showSymbolLogo": true,
			"colorTheme": "light",
			"isTransparent": false,
			"displayMode": "adaptive",
			"locale": "en"
		}
			</script>
		</div> -->
	</section>


























	<!--Video Section-->
	<!-- <section class="content-section video-section ulockd-pdng0">
		<div class="pattern-overlay">
			<a id="bgndVideo" class="player" data-property="{videoURL:'https://www.youtube.com/watch?v=w0RGnXpf9LU',containment:'.video-section', quality:'large', autoPlay:true, mute:true, opacity:1}">bg</a>
		    <div class="container">
		    	<div class="row">
			        <div class="col-md-12 text-center">
				        <h1 class="color-white" style="color: white;">Invest with us safely</h1>
				        <h3 class="color-white" style="color: white;">Be  Smart  Be Safe </h3>
				    </div>
		    	</div>
		    </div>
		</div>
	</section> -->
	<!--Video Section Ends Here-->

	<!-- Our About -->
	<!-- <section class="ulockd-about ulockd-pad12650">

		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="about-box">
						<h2 class="ulockd-mrgn120 title-bottom"> {{$get_note->first()->name}}</h2>

						<p class="lead">{{$get_note->first()->title}}</p>
						<p>{{$get_note->first()->description}}</p>
						<img class="img-responsive h100" src="{{url('website/images/resource/signeture.png')}}" alt="signeture.png">
					</div>
				</div>
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12">
							<div class="three-grid-slider">

								@foreach ($get_storys as $get_story)
								<div class="item">
									<div class="about-box2 text-center">
										<div class="ab-thumb">
											<img class="img-responsive img-whp" style="height:320px;width:430px" src="{{url('/uplodes/newsphoto/'.@$get_story->image)}}" alt="s1.jpg">
											<div class="about-icon text-center"><i class="flaticon-classroom text-thm2"></i></div>
										</div>


										<div class="ab-details">
											<h4 class="fwb">{{$get_story->name}}</h4>
											<a class="btn btn-md ulockd-btn-thm2" href="{{url(LaravelLocalization::setLocale().'/Articles/'.$get_story->id)}}"> اقراء المزيد</a>
										</div>





									</div>
								</div>
		@endforeach








							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- TradingView Widget BEGIN -->
		<div class="tradingview-widget-container">
			<div class="tradingview-widget-container__widget"></div>
			<script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-ticker-tape.js" async>
			{
			"symbols": [
				{
					"proName": "FOREXCOM:SPXUSD",
					"title": "S&P 500"
				},
				{
					"proName": "FOREXCOM:NSXUSD",
					"title": "Nasdaq 100"
				},
				{
					"proName": "FX_IDC:EURUSD",
					"title": "EUR/USD"
				},
				{
					"proName": "BITSTAMP:BTCUSD",
					"title": "BTC/USD"
				},
				{
					"proName": "BITSTAMP:ETHUSD",
					"title": "ETH/USD"
				}
			],
			"showSymbolLogo": true,
			"colorTheme": "light",
			"isTransparent": false,
			"displayMode": "adaptive",
			"locale": "en"
		}
			</script>
		</div>
		<!-- TradingView Widget END -->
	</section>

	@endif









	<!-- Our Blog -->
	<section class="ulockd-blog ulockd_bgp1">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center">
					<div class="ulockd-main-title">
						<h2>{{__('website.OurNews')}} </h2>
						<p>{{__('website.latterOurNews')}} </p>
					</div>
				</div>
			</div>



			<div class="row">
				<div class="col-xxs-12 col-xs-12 col-sm-12 col-md-12">

					<div class="three-grid-slider">


@foreach ($get_news as $get_new)
						<div class="item">
								@if(\Mcamara\LaravelLocalization\Facades\LaravelLocalization::getCurrentLocale()=='ar')
								<article class="blog_post2 text-right">
								@else
								<article class="blog_post2 text-left">
								@endif
			                    <div class="post_review">
			                    	<img class="img-responsive img-whp"   src="{{url('/uplodes/newsphoto/'.@$get_new->image)}}" alt="3.jpg">

			                        <h4 class="post_title"><a target="_blank"  href="{{url(LaravelLocalization::setLocale().'/Forex_Details/'.$get_new->id)}}">{{ Str::limit($get_new->title, 80) }} </a></h4>
			                        <ul class="post_comment list-inline">
			                            <li>By<a href="#"> <i class="fa fa-user"></i></a>Admin</li>
			                            <li>0<a href="#"> comment</a></li>
			                        </ul>
			                    </div>
			                </article>
						</div>
		@endforeach


					</div>



		        </div>

			</div>




		</div>

	</section>













	<!-- Our Blog -->
	<section class="ulockd-blog bgc-snowshade2 ">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center">
					<div class="ulockd-main-title">
						<h2>{{__('website.live')}}</h2>
						<p>{{__('website.latterlive')}}</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-8">
					<div class="two-grid-slider">
						<div class="item">
							<article class="blog_post2 text-left">
			                    <div class="post_review">
			                    	<img class="img-responsive img-whp" src="{{ asset('assets/website/images/blog/1.jpg')}}" alt="1.jpg">
			                        <div class="post_date ulockd-bgthm">12 <small>feb</small></div>
			                        <h4 class="post_title"><a href="https://www.facebook.com/Smart-Deal-109928967370676/videos/678276629780944/">Lorem ipsum dolor sit amet, consectetur</a></h4>
			                        <ul class="post_comment list-inline">
			                            <li>By<a href="#"> <i class="fa fa-user"></i></a>Admin</li>
			                            <li>0<a href="#"> comment</a></li>
			                        </ul>
			                    </div>
			                </article>
						</div>
						<div class="item">
							<article class="blog_post2 text-left">
			                    <div class="post_review">
			                    	<img class="img-responsive img-whp" src="{{ asset('assets/website/images/blog/2.jpg')}}" alt="2.jpg">
			                        <div class="post_date ulockd-bgthm">12 <small>feb</small></div>
			                        <h4 class="post_title"><a href="https://www.facebook.com/Smart-Deal-109928967370676/videos/678276629780944/">Lorem ipsum dolor sit amet, consectetur</a></h4>
			                        <ul class="post_comment list-inline">
			                            <li>By<a href="#"> <i class="fa fa-user"></i></a>Admin</li>
			                            <li>0<a href="#"> comment</a></li>
			                        </ul>
			                    </div>
			                </article>
						</div>
						<div class="item">
							<article class="blog_post2 text-left">
			                    <div class="post_review">
			                    	<img class="img-responsive img-whp" src="{{ asset('assets/website/images/blog/3.jpg')}}" alt="3.jpg">
			                        <div class="post_date ulockd-bgthm">12 <small>feb</small></div>
			                        <h4 class="post_title"><a href="https://www.facebook.com/Smart-Deal-109928967370676/videos/678276629780944/">Lorem ipsum dolor sit amet, consectetur</a></h4>
			                        <ul class="post_comment list-inline">
			                            <li>By<a href="#"> <i class="fa fa-user"></i></a>Admin</li>
			                            <li>0<a href="#"> comment</a></li>
			                        </ul>
			                    </div>
			                </article>
						</div>
					</div>
		        </div>
				<div class="col-md-4">
					<article class="blog_post2">
	                    <div class="post_review">
	                        <div class="post_date ulockd-bgthm">12 <small>feb</small></div>
	                        <h4 class="post_title"><a href="#">Lorem ipsum dolor sit amet, consectetur</a></h4>
	                        <ul class="post_comment list-inline">
	                            <li>By<a href="#"> <i class="fa fa-user"></i></a>Admin</li>
	                            <li>0<a href="#"> comment</a></li>
	                        </ul>
	                    </div>
	                </article>
					<article class="blog_post2 ulockd-mrgn1220">
	                    <div class="post_review">
	                        <div class="post_date ulockd-bgthm">12 <small>feb</small></div>
	                        <h4 class="post_title"><a href="#">Lorem ipsum dolor sit amet, consectetur</a></h4>
	                        <ul class="post_comment list-inline">
	                            <li>By<a href="#"> <i class="fa fa-user"></i></a>Admin</li>
	                            <li>0<a href="#"> comment</a></li>
	                        </ul>
	                    </div>
	                </article>
					<article class="blog_post2 ulockd-mrgn1220">
	                    <div class="post_review">
	                        <div class="post_date ulockd-bgthm">12 <small>feb</small></div>
	                        <h4 class="post_title"><a href="#">Lorem ipsum dolor sit amet, consectetur</a></h4>
	                        <ul class="post_comment list-inline">
	                            <li>By<a href="#"> <i class="fa fa-user"></i></a>Admin</li>
	                            <li>0<a href="#"> comment</a></li>
	                        </ul>
	                    </div>
	                </article>
				</div>
			</div>
		</div>
	</section>





	<!-- Our Report -->
	<section class="freequently-ask  ulockd_bgp1">
		<div class="container">
			<div class="row">


					<div class="col-md-6 col-md-offset-3 text-center">
						<div class="ulockd-main-title">
							<h2>{{__('website.report')}}</h2>
							<p>{{__('website.latterreport')}}</p>
						</div>
					</div>

				<div class="col-md-6">
					<div class="about-box">
						<h2 class="ulockd-mrgn120">{{$get_report->title}}</h2>


						<p>  @php
	 echo @$get_report->description;
	@endphp
</p>
						<a href="{{url(LaravelLocalization::setLocale().'/AllReports/')}}" class="btn btn-default ulockd-btn-thm2">Read More</a>
					</div>
				</div>
				<div class="col-md-6">
					<div class="about-box">


						@if(!$get_report->link =="")
						<iframe width="420" height="315"
					 src="{{$get_report->link}}">
				 </iframe>
				 @else
			 <img class="img-responsive img-whp" src="{{url('/uplodes/newsphoto/'.@$get_report->image)}}" alt="3.png">

         @endif




					</div>
				</div>
			</div>
		</div>
	</section>













	<!-- Our Blog -->
	<section class="ulockd-blog bgc-snowshade2 ">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center">
					<div class="ulockd-main-title">
						<h2> {{__('website.MarketAnalysis')}}   </h2>
					</div>
				</div>
			</div>
			<div class="row">



				@foreach ($news_techical_analyses as $news_techical_analyse)



								<div class="col-lg-6">
									<article class="row blog_post_one">
										<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-6">
											<div class="post_thumb">
												<img class="img-responsive img-whp thumbnail"    src="{{url('/uplodes/newsphoto/'.@$news_techical_analyse->image)}}" alt="1.jpg">
											</div>
										</div>
										<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-6 ulockd-pdng0">
											<div class="post_details">
												<h3> <span>{{$news_techical_analyse->todays}} /</span> <small>{{$news_techical_analyse->months}} {{$news_techical_analyse->years}}</small></h3>
												<ul class="list-inline">
													<li><i class="fa fa-comment text-thm2"></i></li>
													<li><a href="#"> 0 Comment</a></li>
													<li><i class="fa fa-heart text-thm2"></i></li>
													<li><a href="#"> 0 Like</a></li>
												</ul>
												<h1 class="post_title"> <p>{{ Str::limit($news_techical_analyse->title, 20) }} </p> </h4>
												<a class="tdu" href="{{url(LaravelLocalization::setLocale().'/AllTechicalAnalysis/'.'.'.$news_techical_analyse->get_categories()->first()->name)}} ">Read More</a>
											</div>
										</div>
									</article>
								</div>
 @endforeach








			</div>
		</div>
	</section>



	<!-- Our Team -->
	<section class="our-team ulockd_bgp1 ">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center">
					<div class="ulockd-main-title">
					<h2>{{__('website.OurTraders')}}</h2>


					</div>
				</div>
			</div>
			<div class="row">

				  @foreach ($get_OurTraders_have_techical_analyses as $get_OurTraders_have_techical_analyse)
				<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-3">
					<div class="team_post">
						<div class="thumb-box">

											<img src="{{url('/uplodes/users/'.@$get_OurTraders_have_techical_analyse->image)}}" alt="{{url('/uplodes/users/'.@$get_OurTraders_have_techical_analyse->image)}}">
											<div class="thumb-box-content">
												<a href="">	<h3 class="title text-thm2">{{$get_OurTraders_have_techical_analyse->name}}</h3> </a>
													<span class="post">- Share</span>
													<ul class="icon">
															<li><a href="#" class="fa fa-linkedin ulockd-bgthm"></a></li>
															<li><a href="#" class="fa fa-facebook ulockd-bgthm"></a></li>
															<li><a href="#" class="fa fa-twitter ulockd-bgthm"></a></li>
															<li><a href="#" class="fa fa-link ulockd-bgthm"></a></li>
													</ul>
											</div>
									</div>
						<div class="team-details bgc-white">
							<a href="{{url(LaravelLocalization::setLocale().'/our/Traders/Details/'.$get_OurTraders_have_techical_analyse->id)}}"> <h3 class="member-name">{{$get_OurTraders_have_techical_analyse->name}}</h3></a>
							<h5 class="member-post">- Our Director</h5>
							<p>Lorem ipsum dolor sit amet, esse consectetur adipisicing elit.</p>
						</div>
					</div>
				</div>
				@endforeach

			</div>
		</div>
	</section>














	<!-- Our Gallery -->
	<section class="our-gallery ulockd_bgp5">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center">
					<div class="ulockd-main-title">
						<h2>Our Gallery</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem labore voluptates consequuntur velit maiores fugiat eaque.</p>
					</div>
				</div>
			</div>
            <div class="row">
	            <div class="col-md-12">
	                <!-- Masonry Filter -->
	                <ul class="list-inline masonry-filter text-center">
	                    <li><a href="#" class="active" data-filter="*">All</a></li>

@foreach($get_Gallerycategorys as $get_Gallerycategory)
	                    <li><a href="#" data-filter=".{{$get_Gallerycategory->name}}" class="">{{$get_Gallerycategory->name}}</a></li>
	@endforeach
	                </ul>
	                <!-- End Masonry Filter -->

	                <!-- Masonry Grid -->
	                <div id="grid" class="masonry-gallery grid-4 clearfix">

		                <!-- Masonry Item -->
		                <!-- <div class="isotope-item experts consulting">
		                    <div class="gallery-thumb">
			                    <img class="img-responsive img-whp" src="images/gallery/9.jpg" alt="project">
			                    <div class="overlayer">
									<div class="lbox-caption">
										<div class="lbox-details">
											<h5>Gallery Title Here</h5>
											<ul class="list-inline">
												<li>
													<a class="popup-img" href="{{ asset('assets/website/images/gallery/index4.jpeg')}}" title="Gallery Photos"><span class="flaticon-add-square-button"></span></a>
												</li>
												<li>
													<a class="link-btn" href="#" ><span class="flaticon-link-symbol"></span></a>
												</li>
											</ul>
										</div>
									</div>
			                    </div>
		                    </div>
		                </div> -->





@foreach($get_Gallerys as $get_Gallery)

										<div class="isotope-item {{$get_Gallery->get_categories()->first()->name}}">
												<div class="gallery-thumb">
													<img class="img-responsive img-whp" src="{{url('/uplodes/newsphoto/'.@$get_Gallery->image)}}" alt="project">
													<div class="overlayer">
									<div class="lbox-caption">
										<div class="lbox-details">
											<h5>{{$get_Gallery->get_categories()->first()->name}}</h5>
											<ul class="list-inline">
												<li>
													<a class="popup-img" href="{{url('/uplodes/newsphoto/'.@$get_Gallery->image)}}" title="Gallery Photos"><span class="flaticon-add-square-button"></span></a>
												</li>
												<li>

												</li>
											</ul>
										</div>
									</div>
													</div>
												</div>
										</div>

@endforeach




		                <!-- Masonry = Masonry Item -->
	                </div>
	                <!-- Masonry Gallery Grid Item -->
	            </div>
            </div>
		</div>
	</section>






	@if(\Mcamara\LaravelLocalization\Facades\LaravelLocalization::getCurrentLocale()=='ar')

	<!-- Our Team -->
	<section class="ulockd-team-two">
		<div class="container">
			<div class="row">
				<div class="col-md-7">
					<h3 class="title-bottom ulockd-mrgn120 ulockd-mrgn635">معلومات  عنا </h3>
					<div class="row">
						<div class="col-xxs-12 col-xs-6 col-sm-6">

							<img class="img-responsive img-whp" src="{{ url('website/images/about/3.jpg')}}" alt="3.jpg">
							<h3>رؤيتنا </h3>
							<p>نسعى ان نكون رواد أعمال في مجالات إستثمارية متعددة لتقديم الاتسويق الإستثماري و إستشارات السوق و تدريب المستثمرين في مختلف المجالات </p>
						</div>
						<div class="col-xxs-12 col-xs-6 col-sm-6">
							<img class="img-responsive img-whp" src="{{ url('website/images/about/2.jpg')}}" alt="2.jpg">
							<h3>مهمتنا </h3>
							<ul class="list-style-square">

								<li class="fz16">سرعة في الإنتشار </li>
								<li class="fz16">تحقيق أعلى عائد إستثماري </li>
								<li class="fz16">الوصول إلي أكبر عدد من المستثمرين </li>

							</ul>
							<p>تجاوز توقعات عملائنا ,نبني أسس علمية في مجالات أستثمارية متعددة </p>
						</div>
					</div>
				</div>
				<div class="col-md-5">
					<h3 class="title-bottom ulockd-mrgn120 ulockd-mrgn635">هل  لديك اى سوال؟</h3>
					<div class="about-box">
						<div class="booking_form_style_home text-center">
											<!-- Booking Form Sart-->
										<form id="booking_form" class="text-center" name="booking_form" action="{{url('RequestAQuote')}}" method="post" novalidate="novalidate">
  {{csrf_field()}}
												<div class="messages"></div>
								<div class="row">
													<div class="col-sm-12">
														<p class="lead"> استشاره  </p>
													</div>
													<div class="col-sm-12">
															<div class="form-group">
											<input id="form_name" name="name" class="form-control form_control" placeholder="اول اسم" required="required" data-error="Name is required." type="text">
											<div class="help-block with-errors"></div>
										</div>
													</div>
													<div class="col-sm-12">
															<div class="form-group">
																<input id="email" name="email" class="form-control form_control required email" placeholder="الايميل" required="required" data-error="Email is required." type="email">
											<div class="help-block with-errors"></div>
															</div>
													</div>
													<div class="col-sm-6">
															<div class="form-group">
																<input id="phone" name="phone" class="form-control form_control required" placeholder="الهاتف" required="required" data-error="Phone Number is required." type="text">
											<div class="help-block with-errors"></div>
										</div>
													</div>
													<div class="col-sm-6">
															<div class="form-group">
																<div class="ulockd-select-style">
												<select id="form_option" class="form-control form_control required booking_form_control" required="required" data-error="Option is required." name="option">
													<option value=""> اختار الخدمه</option>
																					<option value="تعليم">تعليم</option>
																					<option value="استشارات">استشارات</option>
																					<option value="توصيات"> توصيات </option>
																					<option value="تحليل فنى"> تحليل فنى</option>
																					<option value="نسخ  صفقات ">نسخ  صفقات  </option>
																					<option value="مشاريعنا"> مشاريعنا  </option>

																		</select>
																</div>
											<div class="help-block with-errors"></div>
															</div>
													</div>
													<div class="col-sm-12">
																	<div class="form-group">
																			<textarea id="form_message" name="message" class="form-control ulockd-form-tb required" rows="5" placeholder="رسالتك" required="required" data-error="Message is required."></textarea>
																			<div class="help-block with-errors"></div>
																	</div>
															<div class="form-group">
																<input name="form-botcheck" class="form-control" type="hidden" value="">
																<button type="submit" class="btn btn-lg ulockd-btn-thm2 btn-block">ارسال</button>
															</div>
													</div>
												</div>
										</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


	@else



	<!-- Our Team -->
	<section class="ulockd-team-two">
		<div class="container">
			<div class="row">
				<div class="col-md-7">
					<h3 class="title-bottom ulockd-mrgn120 ulockd-mrgn635">About Our Info</h3>
					<div class="row">
						<div class="col-xxs-12 col-xs-6 col-sm-6">

							<img class="img-responsive img-whp" src="{{ url('website/images/about/3.jpg')}}" alt="3.jpg">
							<h3>Our Vision</h3>
							<p>Consectetur adipisicing elit. Aliquam totam cupiditate iste doloribus, unde minima vero quidem. Porro, laborum obcaecati architecto ex nostrum doloremque magni. Culpa sunt, ex nostrum doloremque incidunt eos atque officia harum impedit tempora.</p>
						</div>
						<div class="col-xxs-12 col-xs-6 col-sm-6">

							<img class="img-responsive img-whp" src="{{ url('website/images/about/2.jpg')}}" alt="2.jpg">
							<h3>Our Mision</h3>
							<ul class="list-style-square">
								<li class="fz16">Safety</li>
								<li class="fz16">Community</li>
								<li class="fz16">Sustainability</li>
							</ul>
							<p>Vero laboriosam aperiam quasi nihil, Culpa sunt repellendus molestiae eos atque officia quaerat quia officiis neque.</p>
						</div>
					</div>
				</div>
				<div class="col-md-5">
					<h3 class="title-bottom ulockd-mrgn120 ulockd-mrgn635">Have Any Questions?</h3>
					<div class="about-box">
						<div class="booking_form_style_home text-center">
											<!-- Booking Form Sart-->
										<form id="booking_form" class="text-center" name="booking_form" action="{{url('RequestAQuote')}}" method="post" novalidate="novalidate">
  {{csrf_field()}}
												<div class="messages"></div>
								<div class="row">
													<div class="col-sm-12">
														<p class="lead">Request A Quote</p>
													</div>
													<div class="col-sm-12">
															<div class="form-group">
											<input id="form_name" name="name" class="form-control form_control" placeholder="Full Name" required="required" data-error="Name is required." type="text">
											<div class="help-block with-errors"></div>
										</div>
													</div>
													<div class="col-sm-12">
															<div class="form-group">
																<input id="form_email" name="email" class="form-control form_control required email" placeholder="Email" required="required" data-error="Email is required." type="email">
											<div class="help-block with-errors"></div>
															</div>
													</div>
													<div class="col-sm-6">
															<div class="form-group">
																<input id="form_phone" name="phone" class="form-control form_control required" placeholder="Phone" required="required" data-error="Phone Number is required." type="text">
											<div class="help-block with-errors"></div>
										</div>
													</div>
													<div class="col-sm-6">
															<div class="form-group">
																<div class="ulockd-select-style">
												<select id="form_option" class="form-control form_control required booking_form_control" required="required" data-error="Option is required." name="option">
													<option value="">Select Service</option>
																					<option value="learn">learn</option>
																					<option value="Consulting">Consulting</option>
																					<option value="Recommendations">Recommendations</option>
																					<option value="Technical analysis">Technical analysis</option>
																					<option value="Copy deals">Copy deals</option>
																					<option value="Our projects">Our projects</option>

																		</select>
																</div>
											<div class="help-block with-errors"></div>
															</div>
													</div>
													<div class="col-sm-12">
																	<div class="form-group">
																			<textarea id="form_message" name="message" class="form-control ulockd-form-tb required" rows="5" placeholder="Your massage" required="required" data-error="Message is required."></textarea>
																			<div class="help-block with-errors"></div>
																	</div>
															<div class="form-group">
																<input name="form-botcheck" class="form-control" type="hidden" value="">
																<button type="submit" class="btn btn-lg ulockd-btn-thm2 btn-block">Send</button>
															</div>
													</div>
												</div>
										</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

		@endif






	<!-- Our Testimonials -->
	<section class="ulockd-testimonial ulockd_bgp2">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center">
					<div class="ulockd-main-title">
						<h2>Our partners</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem labore voluptates consequuntur velit maiores fugiat eaque.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-5">
		            <div class="testimonial_tab" role="tabpanel">
		                <!-- Tab panes -->
		                <div class="tab-content" id="tabs-collapse">
		                    <div role="tabpanel" class="tab-pane fade in active" id="finPress">
		                        <div class="tab-inner">
		                            <p>Cras convallis finibus porta. Integer in ligula leo. Cras quis nisl, at sapien. Mauris ultricies nisi eget velit bibendum, Cum sociis natoque penatibus et magnis dis montes, nascetur mus.</p>
		                            <hr>
		                            <h4 class="text-uppercase">Dustin Lamont</h4>
		                            <p><em class="text-capitalize"> Senior web developer</em> at <a href="#">Apple</a></p>
		                        </div>
		                    </div>
		                    <div role="tabpanel" class="tab-pane fade" id="daksh">
		                        <div class="tab-inner">
		                            <p>Cras convallis finibus porta. Integer in ligula leo. Cras quis nisl, at sapien. Mauris ultricies nisi eget velit bibendum, Cum sociis natoque penatibus et magnis dis montes, nascetur mus.</p>
		                            <hr>
		                            <h4 class="text-uppercase">Daksh Bhagya</h4>
		                            <p><em class="text-capitalize"> UX designer</em> at <a href="#">Google</a></p>
		                        </div>
		                    </div>
		                    <div role="tabpanel" class="tab-pane fade" id="anna">
		                        <div class="tab-inner">
		                            <p>Cras convallis finibus porta. Integer in ligula leo. Cras quis nisl, at sapien. Mauris ultricies nisi eget velit bibendum, Cum sociis natoque penatibus et magnis dis montes, nascetur mus.</p>
		                            <hr>
		                            <h4 class="text-uppercase">Anna Pickard</h4>
		                            <p><em class="text-capitalize"> Master web developer</em> at <a href="#">Intel</a></p>
		                        </div>
		                    </div>
		                    <div role="tabpanel" class="tab-pane fade" id="wafer">
		                        <div class="tab-inner">
		                            <p>Cras convallis finibus porta. Integer in ligula leo. Cras quis nisl, at sapien. Mauris ultricies nisi eget velit bibendum, Cum sociis natoque penatibus et magnis dis montes, nascetur mus.</p>
		                            <hr>
		                            <h4 class="text-uppercase">Wafer Baby</h4>
		                            <p><em class="text-capitalize"> Web designer</em> at <a href="#">Microsoft</a></p>
		                        </div>
		                    </div>
		                </div>
		                <!-- Nav tabs -->
		                <ul class="nav nav-justified" id="nav-tabs" role="tablist">
		                    <li role="presentation" class="active">
		                        <a href="#finPress" aria-controls="finPress" role="tab" data-toggle="tab">
		                            <img class="img-circle" src="{{ url('website/images/testimonial/1.jpg')}}" alt="1.jpg">
		                            <span class="quote ulockd-bgthm"><i class="fa fa-quote-left"></i></span>
		                        </a>
		                    </li>
		                    <li role="presentation" class="">
		                        <a href="#daksh" aria-controls="daksh" role="tab" data-toggle="tab">
		                            <img class="img-circle" src="{{ url('website/images/testimonial/2.jpg')}}" alt="2.jpg">
		                            <span class="quote ulockd-bgthm"><i class="fa fa-quote-left"></i></span>
		                        </a>
		                    </li>
		                    <li role="presentation" class="">
		                        <a href="#anna" aria-controls="anna" role="tab" data-toggle="tab">
		                            <img class="img-circle" src="{{ url('website/images/testimonial/3.jpg')}}" alt="3.jpg">
		                            <span class="quote ulockd-bgthm"><i class="fa fa-quote-left"></i></span>
		                        </a>
		                    </li>
		                    <li role="presentation" class="">
		                        <a href="#wafer" aria-controls="wafer" role="tab" data-toggle="tab">
		                            <img class="img-circle" src="{{ url('website/images/testimonial/4.jpg')}}" alt="4.jpg">
		                            <span class="quote ulockd-bgthm"><i class="fa fa-quote-left"></i></span>
		                        </a>
		                    </li>
		                </ul>
		            </div>
		        </div>
				<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-7">
					<div class="row">
						<div class="col-xs-6 col-sm-6 col-md-4 ulockd-pdng0"><div class="partner"><img src="{{ url('website/images/partners/1.png')}}" alt="1.png"></div></div>
						<div class="col-xs-6 col-sm-6 col-md-4 ulockd-pdng0"><div class="partner"><img src="{{ url('website/images/partners/2.png')}}" alt="2.png"></div></div>
						<div class="col-xs-6 col-sm-6 col-md-4 ulockd-pdng0"><div class="partner"><img src="{{ url('website/images/partners/3.png')}}" alt="3.png"></div></div>
						<div class="col-xs-6 col-sm-6 col-md-4 ulockd-pdng0"><div class="partner"><img src="{{ url('website/images/partners/4.png')}}" alt="4.png"></div></div>
						<div class="col-xs-6 col-sm-6 col-md-4 ulockd-pdng0"><div class="partner"><img src="{{ url('website/images/partners/5.png')}}" alt="5.png"></div></div>
						<div class="col-xs-6 col-sm-6 col-md-4 ulockd-pdng0"><div class="partner"><img src="{{ url('website/images/partners/6.png')}}" alt="5.png"></div></div>
					</div>
		        </div>
			</div>
		</div>
	</section>







@include('website.includes.footer')

<!-- <a class="scrollToHome ulockd-bgthm" href="#"><i class="fa fa-home"></i></a> -->
</div>

<!-- Wrapper End -->
@include('website.includes.js')


</html>



















<!--

      <!DOCTYPE html>
      <html lang="en">
      	<head>
      		<meta charset="UTF-8" />
      		<meta
      			name="viewport"
      			content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"
      		/>
      		<meta http-equiv="X-UA-Compatible" content="ie=edge" />
      		<title>Jodit Test Document</title>
      		<link rel="stylesheet" href="./app.css" />
      		<link rel="stylesheet" href="./build/jodit.min.css" />
      		<script src="./build/jodit.js"></script>




              <script src="{{ asset('editor/build/jodit.js')}}"></script>
              <script src="{{ asset('editor/js/sample.js')}}"></script>
              <script src="{{ asset('editor/build/jodit.js')}}"></script>

                <link rel="stylesheet" type="text/css" href="{{ asset('editor/app.css')}}" />
              	<link rel="stylesheet" type="text/css" href="{{ asset('editor/build/jodit.min.css')}}" />



      	</head>
      	<body>
      		<style>
      			#box {
      				padding: 100px;
      				margin: 20px;
      				position: relative;
      				height: 500px;
      			}

      			@media (max-width: 480px) {
      				#box {
      					padding: 0;
      				}
      			}
      		</style>
      		<div id="box">
      			<h1>Jodit Test Document</h1>
      			<textarea id="editor">
      				&lt;img src="https://xdsoft.net/jodit/build/images/artio.jpg"/&gt;
      			</textarea>
      		</div>
      		<script>
      			const editor = Jodit.make('#editor' ,{
      				uploader: {
      					url: 'https://xdsoft.net/jodit/connector/index.php?action=fileUpload'
      				},
      				filebrowser: {
      					ajax: {
      						url: 'https://xdsoft.net/jodit/connector/index.php'
      					}
      				}
      			});
      		</script>
      	</body>
      </html> -->
