<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- css file -->
@include('website.includes.css')
<title>BonaZa - Business and Finance HTML Template</title>
<!-- Favicon -->
<link href="images/favicon.ico" sizes="128x128" rel="shortcut icon" type="image/x-icon" />
<link href="images/favicon.ico" sizes="128x128" rel="shortcut icon" />

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="wrapper">
	<div id="preloader" class="preloader">
		<div id="pre" class="preloader_container"><div class="preloader_disabler btn btn-default">Disable Preloader</div></div>
	</div>
  @include('website.includes.header')


	<!-- Home Design Inner Pages -->
	<div class="ulockd-inner-home">
		<div class="container text-center">
			<div class="row">
				<div class="inner-conraimer-details">
					<div class="col-md-12">
						<h1 class="text-uppercase">TEAM</h1>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Home Design Inner Pages -->
	<div class="ulockd-inner-page">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="ulockd-icd-layer">
						<ul class="list-inline ulockd-icd-sub-menu">
							<li><a href="#"> HOME </a></li>
							<li><a href="#"> > </a></li>
							<li> <a href="#"> Signal </a> </li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<section class="ulockd-team">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 text-center">
					<div class="ulockd-main-title">
						<h2 class="text-uppercase">OUR <span class="text-thm2">Signal</span></h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem labore voluptates consequuntur velit necessitatibus maiores fugiat eaque.</p>
					</div>
				</div>
			</div>

			<div class="row ulockd-mrgn1240">



				<div class="col-xs-12 col-sm-6 col-md-3">
					<div class="team-member-two">
						<div class="ulockd-tm-thumb">
							<img class="img-responsive img-whp" src="images/team/8.jpg" alt="8.jpg">
							<div class="team-two-details">
								<div class="tm-name">Smith Johnson</div>
								<h5 class="tm-post">-  Housing Supportive</h5>
								<ul class="list-inline team-sicon ulockd-bgthm">
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
									<li><a href="#"><i class="fa fa-pinterest"></i></a></li>
									<li><a href="#"><i class="fa fa-instagram"></i></a></li>
								</ul>
								<ul class="list-unstyled team-contact-info ulockd-bgthm">
									<li>Fax: 1-888-hpeffaxme</li>
									<li>Phone: +98-9875-5968-54</li>
									<li>Mail: sjhson@mail.se</li>
								</ul>
							</div>
						</div>
					</div>
				</div>


        <div class="col-xs-12 col-sm-6 col-md-3">
          <div class="team-member-two">
            <div class="ulockd-tm-thumb">
              <img class="img-responsive img-whp" src="images/team/8.jpg" alt="8.jpg">
              <div class="team-two-details">
                <div class="tm-name">Smith Johnson</div>
                <h5 class="tm-post">-  Housing Supportive</h5>
                <ul class="list-inline team-sicon ulockd-bgthm">
                  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                  <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                  <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                </ul>
                <ul class="list-unstyled team-contact-info ulockd-bgthm">
                  <li>Fax: 1-888-hpeffaxme</li>
                  <li>Phone: +98-9875-5968-54</li>
                  <li>Mail: sjhson@mail.se</li>
                </ul>
              </div>
            </div>
          </div>
        </div>



			</div>
		</div>
	</section>

@include('website.includes.footer')

<!-- <a class="scrollToHome" href="#"><i class="fa fa-home"></i></a> -->
</div>
<!-- Wrapper End -->
@include('website.includes.js')
</body>

</html>
