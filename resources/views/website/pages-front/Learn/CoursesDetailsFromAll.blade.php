<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
@include('website.includes.css')
<!-- Title -->
<title>smart-deal24.com- {{$title}}</title>
<!-- Favicon -->
<link href="images/favicon.ico" sizes="128x128" rel="shortcut icon" type="image/x-icon" />
<link href="images/favicon.ico" sizes="128x128" rel="shortcut icon" />
<meta name="csrf-token" content="{{ csrf_token() }}">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="wrapper">
	<div id="preloader" class="preloader">
		<div id="pre" class="preloader_container"><div class="preloader_disabler btn btn-default">Disable Preloader</div></div>
	</div>

@include('website.includes.header')
	<!-- Inner Pages Main Section -->
	<section class="ulockd-service-details">
		<div class="container">
			<div class="col-md-4 col-lg-3 ulockd-pdng0">
				<div class="widget-sidebar">
					<!-- <h3 class="title-widget-sidebar ulockd-bb-dashed">Custom Search field</h3> -->
		            <!-- <div id="custom-search-input">
		                <div class="input-group">
		                    <input type="text" class="form-control input-lg bdrs0" placeholder="Search..." />
		                    <span class="input-group-btn">
		                        <button class="btn btn-lg ulockd-btn-thm2" type="button">
		                            <i class="glyphicon glyphicon-search"></i>
		                        </button>
		                    </span>
		                </div>
		            </div> -->
					<div class="ulockd-all-service">
						<h3 class="title-widget-sidebar ulockd-bb-dashed"><span class="flaticon-command text-thm2"></span> All Courses</h3>






	@if(\Mcamara\LaravelLocalization\Facades\LaravelLocalization::getCurrentLocale()=='ar')
						@foreach($learns as $learn)

					 						<div class="list-group">



					                 <div class="last-post" >
					                     <button class="sw-accordion ulockd-bgthm" style="text-align: right;">{{@$learn->name}}</button>
					                   <div class="swa-panel">

					 										@foreach ($learn->get_lesson as $lesson)

					                       <li  class="recent-post" >
					     <button  value="/{{ $lesson->id }}" class="list-group-item" style="text-align: right;">{{ @$lesson->title}}</button>

																</li>

					 									@endforeach

					                   </div>


					                 </div>


					 						</div>

					 @endforeach
@else
@foreach($learns as $learn)

					<div class="list-group">



							 <div class="last-post" >
									 <button class="sw-accordion ulockd-bgthm" >{{@$learn->name}}</button>
								 <div class="swa-panel">

									@foreach ($learn->get_lesson as $lesson)

										 <li  class="recent-post" >
	 <button  value="/{{ $lesson->id }}" class="list-group-item" >{{ @$lesson->title}}</button>

										</li>

								@endforeach

								 </div>


							 </div>


					</div>

@endforeach

@endif
					</div>






					<div class="ulockd-lp">
						<div class="ulockd-latest-post">

						</div>

					</div>

				</div>
			</div>





			<div class="col-md-8 col-lg-9">
				<div class="row">
					<div class="col-md-12 ulockd-mrgn1210">
						<div class="ulockd-project-sm-thumb">
							<img class="img-responsive img-whp" style="height:400px;max-width:1000px;width:" id="image_data" src="{{url('/uplodes/newsphoto/'.@$learns->first()->image)}}" alt="images">
						</div>
					</div>
				</div>



				<div class="row">
					<div class="col-md-12 ulockd-mrgn1210">

						<article class="ulockd-pd-content">
							<div class="ulockd-bp-date">
								<ul class="list-inline">
									<li class="ulockd-bp-date-innner">On <a href="#" id="date"><span class="text-thm2" id="todays">{{$learns->first()->todays}}</span> /  {{$learns->first()->months}} {{$learns->first()->years}}</a></li>
									<li class="ulockd-bp-comment"><a href="#"><span class="flaticon-nurse-head text-thm1"></span> Admin</a></li>
									<li class="ulockd-bp-comment"><a href="#"><span class="flaticon-chat text-thm1"></span> 0 Comment</a></li>
								</ul>
							</div>
							<h3 id="title">{{@$learns->first()->title}}</h3>



<div class='project-dp-one' id='body'>{!! @$learns->first()->get_lesson->first()->body !!}</div>



						</article>

					</div>



				</div>








				<!-- <div class="col-md-12 text-center">
					<nav aria-label="Page navigation navigation-lg">
					    <ul class="pagination">
					    	<li>
					        	<a href="#" aria-label="Previous">
					        		<span aria-hidden="true">PREV</span>
					        	</a>
					    	</li>
					    	<li class="active"><a href="#">1</a></li>
					    	<li><a href="#">2</a></li>
					    	<li><a href="#">3</a></li>
					    	<li><a href="#">4</a></li>
					    	<li><a href="#">5</a></li>
					    	<li>
					        	<a href="#" aria-label="Next">
					        		<span aria-hidden="true">NEXT</span>
					        	</a>
					    	</li>
					    </ul>
					</nav>
				</div> -->
				</div>
			</div>
		</div>
	</section>

@include('website.includes.footer')

<!-- <a class="scrollToHome ulockd-bgthm" href="#"><i class="fa fa-home"></i></a> -->
</div>
<!-- Wrapper End -->


@include('website.includes.js')


<script type="text/javascript">

// $(document).ready(function(){
//
//     $('#countires').change(function(){
//
//          var id = $(this).val();
//           $.ajax({
//               url: baseUrl+"/get-cities/" + id,
//               method: "GET",
//               success:function(data){
//               $('#show_cities').html(data.html);
//               }
//          });
//     });
//
// });



		jQuery('.list-group-item').click(function(e){
			 e.preventDefault();
			 var id = $(this).val();

			 $.ajaxSetup({
					headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
			});
			 jQuery.ajax({
					url: "{{url(LaravelLocalization::setLocale().'/Courses_Details/')}}"+id,
					method: 'post',
					dataType: 'json',
					data: {
						 id: jQuery('#countires').val(),

					},
					success:function(response){

						 $('#title').text(response.data[0].title)

						 $('#body').html(response.data[0].body)

						  $("#image_data").attr("src", '/uplodes/newsphoto/'+response.data[0].image)

						 $('#date').text(response.data[0].todays +'/'+ response.data[0].months +'/'+ response.data[0].years)

						// $(data).html(title);

					}});
			 });

</script>

</body>

</html>
