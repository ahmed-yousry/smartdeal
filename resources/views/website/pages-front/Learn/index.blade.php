<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
@include('website.includes.css')
<!-- Title -->
<title>smart-deal24.com - {{$title}}</title>
<!-- Favicon -->
<link href="images/favicon.ico" sizes="128x128" rel="shortcut icon" type="image/x-icon" />
<link href="images/favicon.ico" sizes="128x128" rel="shortcut icon" />

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="wrapper">
	<div id="preloader" class="preloader">
		<div id="pre" class="preloader_container"><div class="preloader_disabler btn btn-default">Disable Preloader</div></div>
	</div>

@include('website.includes.header')
	<!-- Inner Pages Main Section -->
	<section class="ulockd-service-details">
		<div class="container">
			<div class="col-md-4 col-lg-3 ulockd-pdng0">
				<div class="widget-sidebar">
					<!-- <h3 class="title-widget-sidebar ulockd-bb-dashed">Custom Search field</h3> -->
		            <!-- <div id="custom-search-input">
		                <div class="input-group">
		                    <input type="text" class="form-control input-lg bdrs0" placeholder="Search..." />
		                    <span class="input-group-btn">
		                        <button class="btn btn-lg ulockd-btn-thm2" type="button">
		                            <i class="glyphicon glyphicon-search"></i>
		                        </button>
		                    </span>
		                </div>
		            </div> -->
					<div class="ulockd-all-service">
						<h3 class="title-widget-sidebar ulockd-bb-dashed"><span class="flaticon-command text-thm2"></span> All Courses</h3>









 @foreach($learns as $learn)
						<div class="list-group">



                <div class="last-post">
                    <button class="sw-accordion ulockd-bgthm">{{$learn->name}}</button>
                  <div class="swa-panel">
										@foreach ($learn->get_courses as $courses)
										@foreach ($courses->get_lesson as $lesson)
                      <li class="recent-post">

                       <a href="{{url(LaravelLocalization::setLocale().'/Courses_Details/'. @$lesson->id)}}" class="list-group-item">{{ @$lesson->title}}</a>
                      </li>
									@endforeach
									@endforeach
                  </div>


                </div>


						</div>

@endforeach














					</div>
	              	<!-- <h3 class="title-widget-sidebar ulockd-bb-dashed"><span class="flaticon-folder text-thm2"></span> Archives</h3> -->
	                <!-- <div class="last-post">
	                    <button class="sw-accordion ulockd-bgthm">21/4/2017</button>
	                    <div class="swa-panel">
	                    	<li class="recent-post">
	                    		<div class="post-img">
	                     			<img class="img-responsive" src="images/blog/1.jpg" alt="1.jpg">
	                        	</div>
	                    		<a href="#"><h6>Excepteur sint occaecat cupi non proident laborum.</h6></a>
	                    		<p><small><i class="fa fa-calendar" data-original-title="" title=""></i> 30 July 2017</small></p>
	                    	</li>
	                    	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	                    </div>
	                </div> -->
	                <!-- <hr> -->
	                <!-- <div class="last-post">
	                    <button class="sw-accordion ulockd-bgthm">5/7/2017</button>
	                	<div class="swa-panel">
	                    	<li class="recent-post">
	                    		<div class="post-img">
	                    			<img class="img-responsive" src="images/blog/2.jpg" alt="2.jpg">
	                    		</div>
	                       		<a href="#"><h6>Excepteur sint occaecat cupi non proident laborum.</h6></a>
	                    		<p><small><i class="fa fa-calendar" data-original-title="" title=""></i> 30 July 2017</small></p>
	                    	</li>
	                  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	                	</div>
	                </div> -->
	                <!-- <hr> -->
	                <!-- <div class="last-post">
	                    <button class="sw-accordion ulockd-bgthm">15/9/2017</button>
	                    <div class="swa-panel">
	                     	<li class="recent-post">
	                    		<div class="post-img">
	                    			<img class="img-responsive" src="images/blog/3.jpg" alt="3.jpg">
	                        	</div>
	                    		<a href="#"><h6>Excepteur sint occaecat cupi non proident laborum.</h6></a>
	                    		<p><small><i class="fa fa-calendar" data-original-title="" title=""></i> 30 July 2017</small></p>
	                    	</li>
	                  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	                    </div>
	                </div> -->
	                <!-- <hr> -->
	                <!-- <div class="last-post">
	                	<button class="sw-accordion ulockd-bgthm">2/3/2017</button>
	                	<div class="swa-panel">
	                    	<li class="recent-post">
	                        	<div class="post-img">
	                    			<img class="img-responsive" src="images/blog/4.jpg" alt="4.jpg">
	                        	</div>
	                        	<a href="#"><h6>Excepteur sint occaecat cupi non proident laborum.</h6></a>
	                        	<p><small><i class="fa fa-calendar" data-original-title="" title=""></i> 30 July 2017</small></p>
	                    	</li>
	                  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
		                </div>
	                </div> -->
					<!-- <h3 class="title-widget-sidebar ulockd-bb-dashed"><span class="flaticon-straight-quotes text-thm2"></span> Testimonial</h3>
					<div class="ulockd-inr-testimonials">
						<p>pain was born and I will give you a complete accounf the system, and expound the actuteachings of the grea</p>
						<div class="ulockd-tm-client-details">
							<div class="ulockd-tm-name">
								<h4 class="text-thm1">Gary Watson - <small> Seo</small></h4>
							</div>
						</div>
					</div> -->
					<!-- <h3 class="title-widget-sidebar ulockd-bb-dashed"><span class="flaticon-calendar text-thm2"></span> Latest Post</h3> -->
					<div class="ulockd-lp">
						<div class="ulockd-latest-post">
							<!-- <div class="media">
							    <div class="media-left pull-left">
							    	<a href="#">
							      	    <img class="media-object thumbnail" src="images/testimonial/1.jpg" alt="1.jpg">
							    	</a>
							    </div>
							    <div class="media-body">
							    	<h4 class="media-heading">Ana Andreea</h4>
							    	I have constantly believed Bclinico  to take care of their patain <a href="#">more...</a>
							    	<strong><a href="#"> 20 Jan 2017 </a></strong>
							    </div>
							</div> -->
						</div>
						<!-- <div class="ulockd-latest-post">
							<div class="media">
							    <div class="media-left pull-left">
							    	<a href="#">
							      	    <img class="media-object thumbnail" src="images/testimonial/2.jpg" alt="2.jpg">
							    	</a>
							    </div>
							    <div class="media-body">
							    	<h4 class="media-heading">Simone Andreea</h4>
							    	I have constantly believed Bclinico  to take care of their patain <a href="#">more...</a>
							    	<strong><a href="#"> 20 Jan 2017 </a></strong>
							    </div>
							</div>
						</div> -->
						<!-- <div class="ulockd-latest-post">
							<div class="media">
							    <div class="media-left pull-left">
							    	<a href="#">
							      	    <img class="media-object thumbnail" src="images/testimonial/3.jpg" alt="3.jpg">
							    	</a>
							    </div>
							    <div class="media-body">
							    	<h4 class="media-heading">Devid Andreea</h4>
							    	I have constantly believed Mr Fix to take care of busines <a href="#">more...</a>
							    	<strong><a href="#"> 20 Jan 2017 </a></strong>
							    </div>
							</div>
						</div> -->
					</div>
					<!-- <div class="ulockd-ip-tag">
						<div class="ulockd-tag-list-title">
							<h3 class="title-widget-sidebar ulockd-bb-dashed"><span class="flaticon-mark text-thm2"></span> Tags List </h3>
						</div>
						<ul class="list-inline ulockd-tag-list-details">
							<li><a href="#">Photoshop</a></li>
							<li><a href="#">Design</a></li>
							<li><a href="#">Tutorial</a></li>
							<li><a href="#">Corses</a></li>
							<li><a href="#">Primum</a></li>
							<li><a href="#">Designtuto</a></li>
							<li><a href="#">Autocad</a></li>
							<li><a href="#">Development</a></li>
						</ul>
					</div> -->
					<!-- <div class="ulockd-ip-flickr">
						<div class="ulockd-flickr-list-title">
							<h3 class="title-widget-sidebar ulockd-bb-dashed"><span class="flaticon-flickr text-thm2"></span> Flickr Feed</h3>
							<div class="flickr-photo"></div>
						</div>
					</div> -->
					<!-- <div class="ulockd-ip-flickr">
						<div class="ulockd-flickr-list-title">
							<h3 class="title-widget-sidebar ulockd-bb-dashed"><span class="flaticon-instagram text-thm2"></span> Instagram Feed</h3>
							<div id="instafeed"></div>
						</div>
					</div> -->
				</div>
			</div>





			<div class="col-md-8 col-lg-9">
				<div class="row">
					<div class="col-md-12 ulockd-mrgn1210">
						<div class="ulockd-project-sm-thumb">
							<img class="img-responsive img-whp" style="height:400px;max-width:1000px;width:"  src="{{url('/uplodes/newsphoto/'.@$learns->first()->get_courses()->first()->image)}}" alt="">
						</div>
					</div>
				</div>



				<div class="row">
					<div class="col-md-12 ulockd-mrgn1210">
						<article class="ulockd-pd-content">
							<div class="ulockd-bp-date">
								<ul class="list-inline">
									<li class="ulockd-bp-date-innner">On <a href="#"><span class="text-thm2">25</span> / Jun 2018</a></li>
									<li class="ulockd-bp-comment"><a href="#"><span class="flaticon-nurse-head text-thm1"></span> Ana D Cruse</a></li>
									<li class="ulockd-bp-comment"><a href="#"><span class="flaticon-chat text-thm1"></span> 05 Comment</a></li>
									<li class="ulockd-bp-comment"><a href="#"><span class="flaticon-black-check-box text-thm1"></span> Service</a></li>
								</ul>
							</div>




							<h3>{{@$learns->first()->get_courses->first()->get_lesson->first()->title}}</h3>
							<p class="project-dp-one">{{@$learns->first()->get_courses->first()->get_lesson->first()->body}}</p>
							<!-- <a class="btn btn-lg ulockd-btn-thm2" href="#"> Read More</a> -->
						</article>
					</div>



				</div>








				<!-- <div class="col-md-12 text-center">
					<nav aria-label="Page navigation navigation-lg">
					    <ul class="pagination">
					    	<li>
					        	<a href="#" aria-label="Previous">
					        		<span aria-hidden="true">PREV</span>
					        	</a>
					    	</li>
					    	<li class="active"><a href="#">1</a></li>
					    	<li><a href="#">2</a></li>
					    	<li><a href="#">3</a></li>
					    	<li><a href="#">4</a></li>
					    	<li><a href="#">5</a></li>
					    	<li>
					        	<a href="#" aria-label="Next">
					        		<span aria-hidden="true">NEXT</span>
					        	</a>
					    	</li>
					    </ul>
					</nav>
				</div> -->
				</div>
			</div>
		</div>
	</section>

@include('website.includes.footer')

<!-- <a class="scrollToHome ulockd-bgthm" href="#"><i class="fa fa-home"></i></a> -->
</div>
<!-- Wrapper End -->
@include('website.includes.js')

</body>

</html>
