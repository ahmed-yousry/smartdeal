<!-- Our Partner -->
<section class="parallax ulockd_bgi4 bgc-overlay-white75 ulockd-pad650" data-stellar-background-ratio="0.3">
  <div class="container">
    <div class="row">
      <div class="col-xs-6 col-sm-4 col-md-2"><div class="ulockd-divider-thumb text-center"><img class="img-responsive" src="{{ url('website/images/partners/1.png')}} " alt="1.png"></div></div>
      <div class="col-xs-6 col-sm-4 col-md-2"><div class="ulockd-divider-thumb text-center"><img class="img-responsive" src="{{ url('website/images/partners/2.png')}}" alt="2.png"></div></div>
      <div class="col-xs-6 col-sm-4 col-md-2"><div class="ulockd-divider-thumb text-center"><img class="img-responsive" src="{{ url('website/images/partners/3.png')}}" alt="3.png"></div></div>
      <div class="col-xs-6 col-sm-4 col-md-2"><div class="ulockd-divider-thumb text-center"><img class="img-responsive" src="{{ url('website/images/partners/4.png')}}" alt="4.png"></div></div>
      <div class="col-xs-6 col-sm-4 col-md-2"><div class="ulockd-divider-thumb text-center"><img class="img-responsive" src="{{ url('website/images/partners/5.png')}}" alt="5.png"></div></div>
      <div class="col-xs-6 col-sm-4 col-md-2"><div class="ulockd-divider-thumb text-center"><img class="img-responsive" src="{{ url('website/images/partners/6.png')}}" alt="6.png"></div></div>
    </div>
  </div>
</section>

<!-- Our Footer -->
<section class="ulockd-footer ulockd-pdng0">
  <div class="container footer-padding">
    <div class="row">
      <div class="col-xxs-12 col-xs-6 col-sm-4 col-md-4">
        <div class="logo-widget tac-xxsd">
          <img src="{{ url('website/images/footer-logo.png')}} " alt="footer-logo.png">
        </div>
      </div>
      <div class="col-xxs-12 col-xs-6 col-sm-4 col-md-4">
        <div class="font-icon-social text-center">
          <ul class="list-inline footer-font-icon">
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-feed"></i></a></li>
            <li><a href="#"><i class="fa fa-google"></i></a></li>
            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
          </ul>
        </div>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4">
        <div class="payment-card tac-xsd">
          <img class="pull-right fn-xsd" src="{{ url('website/images/resource/payment.png')}} " alt="payment.png">
        </div>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-3">
        <div class="widget-about">
          <h3>About Us</h3>
          <p>Regardless of whether you need to stay in your own house, are searching for help with a more established relative, looking for exhortation on paying for development, we can help you.</p>
          <p>Doloribus, placeat, minima. Harum voluptatibus quae tempora est, cupiditate culpa, molestiae, illo beatae reiciendis, nisi sit dolores.</p>
        </div>
          <div class="ulockd-footer-newsletter">
            <h3 class="title">News Letter</h3>
                  <form class="ulockd-mailchimp">
                      <div class="input-group">
                        <input type="email" class="form-control input-md" placeholder="Your email" name="EMAIL" value="">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-md"><i class="icon flaticon-right-arrow"></i></button>
                        </span>
                      </div>
                  </form>
          </div>
      </div>
      <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-3">
        <div class="twitter-widget">
          <h3>Twitter Feed</h3>
          <div class="twitter"></div>
        </div>
      </div>
      <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-3">
        <div class="news-widget">
          <h3>Latest News</h3>
          <div class="ulockd-media-box">
            <div class="media">
                <div class="media-left pull-left">
                  <a href="#">
                    <img class="media-object" src="{{ url('website/images/blog/s1.jpg')}} " alt="s1.jpg">
                  </a>
                </div>
                <div class="media-body">
                  <h4 class="media-heading">Let's Move With Blog </h4>
                <a href="#" class="post-date">21 January, 2018</a>
                  <p>Lorem ipsum dolor sit amet, consectetur...</p>
                </div>
            </div>
            <div class="media">
                <div class="media-left pull-left">
                  <a href="#">
                    <img class="media-object" src="{{ url('website/images/blog/s2.jpg')}} " alt="s2.jpg">
                  </a>
                </div>
                <div class="media-body">
                  <h4 class="media-heading">Let's Move With Blog </h4>
                <a href="#" class="post-date">21 January, 2018</a>
                  <p>Lorem ipsum dolor sit amet, consectetur...</p>
                </div>
            </div>
          </div>
        </div>
        <div class="tag-widget">
          <h3>Tag Widget</h3>
          <ul class="list-inline">
            <li><a href="#">Bclinico</a></li>
            <li><a href="#">bMax</a></li>
            <li><a href="#">Comfort Home</a></li>
            <li><a href="#">Be aHand</a></li>
            <li><a href="#">Be Finance</a></li>
            <li><a href="#">GreenPlats</a></li>
            <li><a href="#">eHospital</a></li>
            <li><a href="#">MrFix</a></li>
            <li><a href="#">Couple Heart</a></li>
          </ul>
        </div>
      </div>
      <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-3">
        <div class="link-widget">
          <h3>Important Link</h3>
          <ul class="list-style-square">
            <li><a href="#"> About Licences</a></li>
            <li><a href="#"> Carrers</a></li>
            <li><a href="#"> Community & Forum</a></li>
            <li><a href="#"> Help & Conditions</a></li>
            <li><a href="#"> Privacy Policy</a></li>
            <li><a href="#"> Terms & Conditions</a></li>
          </ul>
        </div>
        <div class="flickr-widget">
          <h3>Recent Work</h3>
          <ul class="list-inline">
            <li>
              <div class="thumb">
                <img alt="flckr1.jpg" src="{{ url('website/images/gallery/flckr1.jpg')}}" class="img-responsive img-whp">
                <div class="overlay">
                  <span class="flaticon-add"></span>
                </div>
              </div>
            </li>
            <li>
              <div class="thumb">
                <img alt="" src="{{ url('website/images/gallery/flckr2.jpg')}} " class="img-responsive img-whp">
                <div class="overlay">
                  <span class="flaticon-add"></span>
                </div>
              </div>
            </li>
            <li>
              <div class="thumb">
                <img alt="" src="{{ url('website/images/gallery/flckr3.jpg')}}" class="img-responsive img-whp">
                <div class="overlay">
                  <span class="flaticon-add"></span>
                </div>
              </div>
            </li>
            <li>
              <div class="thumb">
                <img alt="" src="{{ url('website/images/gallery/flckr4.jpg')}}" class="img-responsive img-whp">
                <div class="overlay">
                  <span class="flaticon-add"></span>
                </div>
              </div>
            </li>
            <li>
              <div class="thumb">
                <img alt="" src="{{ url('website/images/gallery/flckr5.jpg')}}" class="img-responsive img-whp">
                <div class="overlay">
                  <span class="flaticon-add"></span>
                </div>
              </div>
            </li>
            <li>
              <div class="thumb">
                <img alt="" src="{{ url('website/images/gallery/flckr6.jpg')}}" class="img-responsive img-whp">
                <div class="overlay">
                  <span class="flaticon-add"></span>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-xxs-12 col-xs-4 col-sm-4 col-md-4">
        <div class="mail-widget text-center">
          <span class="icon fa fa-envelope"></span>
          <h3>Mail Us</h3>
          <p>info@smart-deal24.com</p>
        </div>
      </div>
      <div class="col-xxs-12 col-xs-4 col-sm-4 col-md-4">
        <div class="call-widget text-center">
          <span class="icon fa fa-phone"></span>
          <h3>Call Us</h3>
          <p>+201009236100</p>
        </div>
      </div>
      <div class="col-xxs-12 col-xs-4 col-sm-4 col-md-4">
        <div class="location-widget text-center">
          <span class="icon fa fa-map-signs"></span>
          <h3>Find Us</h3>
          <p>Fleming, Alexandria.</p>
        </div>
      </div>
    </div>
    <hr class="ulockd-mrgn60">
  </div>
  <div class="ulockd-copy-right">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <p class="color-white">SmartDeal Copyright© 2020. All right reserved.</p>
        </div>
      </div>
    </div>
  </div>
</section>
